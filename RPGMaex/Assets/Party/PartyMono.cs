using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PartyMono : MonoBehaviour
{
    public Party party;
    static GameObject instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this.gameObject;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void shopping(float money)
    {
        print(money);
        party.money += money;
    }

    public void addItem(RPGItem item)
    {
        print(item);
        party.inventory.addItem(item);
    }

    public void removeItem(RPGItem item)
    {
        print(item);
        party.inventory.removeItem(item);
    }
}
