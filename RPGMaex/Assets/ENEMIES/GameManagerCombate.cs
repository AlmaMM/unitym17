using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Xml;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.VFX;
using Random = UnityEngine.Random;

public class GameManagerCombate : MonoBehaviour
{
    [Serialize]
    public GameObject enemy;
    List<Enemies> listEnemyMono;
    GameObject[] recibirEnemySelected;
    GameObject instantiateEnemy;
    int numEnemies;
    GameObject objetivoEnemigo1, objetivoEnemigo2;
    bool can = false;
    bool turnoDos = false;
    string accion1 = null;
    string accion2 = null;
    Dictionary<string, float> ordenSteps = new Dictionary<string, float>();

    public PlayableCharacters prota1, prota2;
    List<GameObject> listEnemies = new List<GameObject>();

    public delegate void pasarNumEnemies(int i);
    public event pasarNumEnemies mNum;

    static GameObject GMC = null;

    void Awake()
    {
        if (GMC == null)
        {
            GMC = this.gameObject;
            SceneManager.sceneLoaded += onSceneLoaded;
            DontDestroyOnLoad(GMC);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    EnemyMono em;

    void onSceneLoaded(Scene s, LoadSceneMode lsm)
    {
        if (s.name == "Combate")
        {
            if(can)
            {
                listEnemies.Clear();
                numEnemies = Random.Range(1, 4);
                mNum?.Invoke(numEnemies);
                instanciarEnemies(numEnemies);
                turnoDos = false;
            }
            else
            {
                instanciarEnemies(3);
                can = true;
            }
            
            listEnemyMono = new List<Enemies>();
            /*
            recibirEnemySelected1 = GameObject.Find("PreFabEnemie(Clone)");
            recibirEnemySelected1.GetComponent<EnemyMono>().mPasarEnemySelected += funcionRecibirEnemy;
            */
            recibirEnemySelected = GameObject.FindGameObjectsWithTag("Enemy");
            for(int i = 0; i<recibirEnemySelected.Count(); i++)
            {
                print(recibirEnemySelected[i]);
                recibirEnemySelected[i].GetComponent<EnemyMono>().mPasarEnemySelected += funcionRecibirEnemy;
            }
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        

    }

    public void funcionRecibirEnemy(Enemies e)
    {
        print(e);
        listEnemyMono.Add(e);

    }

    private void instanciarEnemies(int i)
    {
        for (int j = 0; j < i; j++)
        {
            instantiateEnemy = Instantiate(enemy);
            instantiateEnemy.transform.position = new Vector2(-1-j, 5.5f-(j*2+2));
            instantiateEnemy.GetComponent<Animator>().runtimeAnimatorController = enemy.GetComponent<Animator>().runtimeAnimatorController;
            listEnemies.Add(instantiateEnemy);
        }
    }

    public void guardarAccion(int v)
    {
        switch (v)
        {
            case 0:
                if(!turnoDos)
                {
                    accion1 = "ataqueFisico";
                }
                else
                {
                    accion2 = "ataqueFisico";
                }
                
                break;
            case 1:
                
                break;
            case 2:
                if (!turnoDos)
                {
                    accion1 = "ataqueParty";
                }
                else
                {
                    accion2 = "ataqueParty";
                }
                break;
            case 3:
                
                break;
        }
    }

    public void guardarObjetivo(int v)
    {
        switch (v)
        {
            case 0:
                if(!turnoDos)
                {
                    objetivoEnemigo1 = listEnemies[0].gameObject;
                    turnoDos = true;
                }
                else
                {
                    objetivoEnemigo2 = listEnemies[0].gameObject;
                    turnoDos = false;
                    goSteps();
                }
                
                break;
            case 1:
                if (!turnoDos)
                {
                    objetivoEnemigo1 = listEnemies[1].gameObject;
                    turnoDos = true;
                }
                else
                {
                    objetivoEnemigo2 = listEnemies[1].gameObject;
                    turnoDos = false;
                    goSteps();
                }

                break;
            case 2:
                if (!turnoDos)
                {
                    objetivoEnemigo1 = listEnemies[2].gameObject;
                    turnoDos = true;
                }
                else
                {
                    objetivoEnemigo2 = listEnemies[2].gameObject;
                    turnoDos = false;
                    goSteps();
                }
                break;
        }
    }

    public void goSteps()
    {
        ordenSteps.Clear();
        
        for(int i = 0; i<listEnemyMono.Count; i++)
        {
            ordenSteps.Add(listEnemyMono[i].m_name+"/"+ i, listEnemyMono[i].m_SPD);
        }
        
        
        ordenSteps.Add(prota1.m_name, prota1.m_SPD);
        ordenSteps.Add(prota2.m_name, prota2.m_SPD);
        
        ordenSteps = ordenSteps.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

        foreach (KeyValuePair<string, float> entry in ordenSteps)
        {
            print(entry.Key+" "+entry.Value);
        }

        acabarTurno();

    }

    public void acabarTurno()
    {
        turnoDos = false;
    }


}
