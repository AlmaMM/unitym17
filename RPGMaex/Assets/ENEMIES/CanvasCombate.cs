using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class CanvasCombate : MonoBehaviour
{
    GameObject boton, boton2, recibirEnemies, recibirTurno; 
    Vector3 p1, p2, p3;
    public Characters stats1, stats2;


    public GameObject cursor, nombre1, hp1, pp1, hp2, pp2, nombre12, hp12, pp12, hp22, pp22;
    bool bat;

    static GameObject canvasObject2 = null;

    void Awake()
    {
        if (canvasObject2 == null)
        {
            canvasObject2 = this.gameObject;
            SceneManager.sceneLoaded += onSceneLoaded;
            DontDestroyOnLoad(canvasObject2);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void onSceneLoaded(Scene s, LoadSceneMode lsm)
    {
        if (s.name == "Combate")
        {
            bat = true;
            this.gameObject.SetActive(true);
            turnoDos = false;
            if(this.transform.GetChild(5).gameObject.activeSelf)
            {
                this.transform.GetChild(5).gameObject.SetActive(false);
            }
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).gameObject);
        }
        else
        {
            bat = false;
            this.gameObject.SetActive(false);

            this.transform.GetChild(3).gameObject.SetActive(true);
            this.transform.GetChild(4).gameObject.SetActive(true);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        p1 = GameObject.Find("Pos1").GetComponent<Transform>().position;
        p2 = GameObject.Find("Pos2").GetComponent<Transform>().position;
        p3 = GameObject.Find("Pos3").GetComponent<Transform>().position;

        recibirEnemies = GameObject.Find("GameManagerCombate");
        recibirEnemies.GetComponent<GameManagerCombate>().mNum += setearMNum;
    }

    public void setearMNum(int i)
    {
        if (i == 2)
        {
            this.transform.GetChild(4).gameObject.SetActive(false);
        }
        if (i == 1)
        {
            this.transform.GetChild(3).gameObject.SetActive(false);
            this.transform.GetChild(4).gameObject.SetActive(false);
        }

    }

    public void Update()
    {
        nombre1.GetComponent<TextMeshProUGUI>().text = stats1.m_name;
        hp1.GetComponent<TextMeshProUGUI>().text = Mathf.Round(stats1.m_CurrentHP)+"/"+ Mathf.Round(stats1.m_MaxHP);
        pp1.GetComponent<TextMeshProUGUI>().text = Mathf.Round(stats1.m_CurrentPP)+"/" + Mathf.Round(stats1.m_MaxPP);

        hp2.GetComponent<TextMeshProUGUI>().text = Mathf.Round(stats2.m_CurrentHP) + "/" + Mathf.Round(stats2.m_MaxHP);
        pp2.GetComponent<TextMeshProUGUI>().text = Mathf.Round(stats2.m_CurrentPP) + "/" + Mathf.Round(stats2.m_MaxPP);

        nombre12.GetComponent<TextMeshProUGUI>().text = stats1.m_name;
        hp12.GetComponent<TextMeshProUGUI>().text = Mathf.Round(stats1.m_CurrentHP) + "/" + Mathf.Round(stats1.m_MaxHP);
        pp12.GetComponent<TextMeshProUGUI>().text = Mathf.Round(stats1.m_CurrentPP) + "/" + Mathf.Round(stats1.m_MaxPP);

        hp22.GetComponent<TextMeshProUGUI>().text = Mathf.Round(stats2.m_CurrentHP) + "/" + Mathf.Round(stats2.m_MaxHP);
        pp22.GetComponent<TextMeshProUGUI>().text = Mathf.Round(stats2.m_CurrentPP) + "/" + Mathf.Round(stats2.m_MaxPP);

        boton2 = EventSystem.current.currentSelectedGameObject;
        cursor.GetComponent<Transform>().position = boton2.GetComponent<Transform>().position;

    }

    public void bufferAcciones(int v)
    {
        if(!turnoDos)
        {
            switch (v)
            {
                case 0:
                    cursor.SetActive(true);
                    boton = EventSystem.current.currentSelectedGameObject;
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(this.transform.GetChild(2).gameObject);
                    break;
                case 1:
                    boton = EventSystem.current.currentSelectedGameObject;
                    EventSystem.current.SetSelectedGameObject(null);
                    this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(true);
                    //EventSystem.current.SetSelectedGameObject(this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).gameObject);
                    //cargar inputs magia, poner a true cursor
                    break;
                case 2:
                    //se ocupa GMC
                    break;
                case 3:
                    boton = EventSystem.current.currentSelectedGameObject;
                    EventSystem.current.SetSelectedGameObject(null);
                    this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).transform.GetChild(0).gameObject.SetActive(true);
                    //EventSystem.current.SetSelectedGameObject(this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).transform.GetChild(0).transform.GetChild(0).gameObject);
                    //que se seleccione el primer item de la bolsa, poner a true cursor
                    break;
                case 4:
                    SceneManager.LoadScene(5);
                    break;

            }
        }
        else if(turnoDos)
        {
            switch (v)
            {
                case 0:
                    cursor.SetActive(true);
                    boton = EventSystem.current.currentSelectedGameObject;
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(this.transform.GetChild(2).gameObject);
                    break;
                case 1:
                    boton = EventSystem.current.currentSelectedGameObject;
                    EventSystem.current.SetSelectedGameObject(null);
                    this.transform.GetChild(5).transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(true);
                    //EventSystem.current.SetSelectedGameObject(this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).gameObject);
                    //cargar inputs magia, poner a true cursor
                    break;
                case 3:
                    boton = EventSystem.current.currentSelectedGameObject;
                    EventSystem.current.SetSelectedGameObject(null);
                    this.transform.GetChild(5).transform.GetChild(0).transform.GetChild(3).transform.GetChild(0).gameObject.SetActive(true);
                    //EventSystem.current.SetSelectedGameObject(this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).transform.GetChild(0).transform.GetChild(0).gameObject);
                    //que se seleccione el primer item de la bolsa, poner a true cursor
                    break;
                case 4:
                    SceneManager.LoadScene(5);
                    break;

            }
        }
        
    }

    bool turnoDos = false;

    public void ponerTurnoDos()
    {
        if(!turnoDos)
        {
            turnoDos = true;
            this.transform.GetChild(5).gameObject.SetActive(true);
            cursor.SetActive(false);
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(this.transform.GetChild(5).transform.GetChild(0).transform.GetChild(0).gameObject);
        }
        else
        {
            cambiarTurno();
        }

    }

    public void cambiarTurno()
    {
        this.transform.GetChild(5).gameObject.SetActive(false);
        turnoDos = false;
        cursor.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).gameObject);
    }

    public void volverEmpezar(InputAction.CallbackContext context)
    {
        if(context.performed && bat && !turnoDos)
        {
            if(cursor.gameObject.activeSelf)
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(boton);
                cursor.SetActive(false);
            }
            else if(this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).gameObject.activeSelf ||
                this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).transform.GetChild(0).gameObject.activeSelf)
            {
                this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(false);
                this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(3).transform.GetChild(0).gameObject.SetActive(false);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(boton);
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(this.transform.GetChild(0).transform.GetChild(0).transform.GetChild(4).gameObject);
            }
            
        }
        else if(context.performed && bat && turnoDos)
        {
            if (cursor.gameObject.activeSelf)
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(boton);
                cursor.SetActive(false);
            }
            else if (this.transform.GetChild(5).transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).gameObject.activeSelf ||
                this.transform.GetChild(5).transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).gameObject.activeSelf)
            {
                this.transform.GetChild(5).transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(false);
                this.transform.GetChild(5).transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(false);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(boton);
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(this.transform.GetChild(5).transform.GetChild(0).transform.GetChild(3).gameObject);
            }
        }
    }
}
