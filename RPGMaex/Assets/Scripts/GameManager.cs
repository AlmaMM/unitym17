using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public InventoryManager inventory;
    static GameObject thisGM = null;

    void Awake()
    {
        if (thisGM == null)
        {
            thisGM = this.gameObject;
            DontDestroyOnLoad(thisGM);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        //Clear inventory only when starting a new game, if else just charge the saved inventory.
        inventory.fullClear();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
