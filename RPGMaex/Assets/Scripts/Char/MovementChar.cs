using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementChar : MonoBehaviour
{
    public Animator anim;
    bool b = true;
    Rigidbody2D rb;
    Coroutine Cor;
    bool ar, ab, de, iz;

    // Start is called before the first frame update
    void Start()
    {
        anim.Play("idleab");
        Cor = StartCoroutine(RandomMove());
        rb = GetComponent<Rigidbody2D>();
    }


    IEnumerator RandomMove()
    {
        int spd = 3;
        while(b)
        {
            yield return new WaitForSeconds(Random.Range(2, 6));
            int r = (int) Random.Range(0, 2);
            if(r==1)
            {
                int dir = (int) Random.Range(1, 5);

                switch(dir)
                {
                    case 1:
                        if(!ar)
                        {
                            ar = true;
                            ab = false;
                            anim.Play("car");
                            rb.velocity = new Vector2(0, spd);
                            yield return new WaitForSeconds(0.4f);
                            rb.velocity = Vector2.zero;
                        }
                        anim.Play("idlear");
                        break;
                    case 2:
                        if (!de)
                        {
                            de = true;
                            iz = false;
                            anim.Play("cde");
                            rb.velocity = new Vector2(spd, 0);
                            yield return new WaitForSeconds(0.4f);
                            rb.velocity = Vector2.zero;
                        }
                        anim.Play("idlede");
                        break;
                    case 3:
                        if(!ab)
                        {
                            ab = true;
                            ar = false;
                            anim.Play("cab");
                            rb.velocity = new Vector2(0, -spd);
                            yield return new WaitForSeconds(0.4f);
                            rb.velocity = Vector2.zero;
                        }
                        anim.Play("idleab");
                        break;
                    case 4:
                        if(!iz)
                        {
                            iz = true;
                            de = false;
                            anim.Play("ciz");
                            rb.velocity = new Vector2(-spd, 0);
                            yield return new WaitForSeconds(0.4f);
                            rb.velocity = Vector2.zero;
                        }
                        anim.Play("idleiz");
                        break;

                }
            }
            else
            {
                int dir = Random.Range(1, 5);

                switch (dir)
                {
                    case 1:
                        anim.Play("idlear");
                        break;
                    case 2:
                        anim.Play("idleab");
                        break;
                    case 3:
                        anim.Play("idlede");
                        break;
                    case 4:
                        anim.Play("idleiz");
                        break;
                }

            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Player")
        {
            StopCoroutine(Cor);
            this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            b = false;
        }
            
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            b = true;
            Cor = StartCoroutine(RandomMove());
        }

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.transform.position.y > this.gameObject.transform.position.y)
        {
            this.GetComponent<SpriteRenderer>().sortingOrder = 2;
            if (collision.gameObject.transform.position.y < (this.gameObject.transform.position.x + 0.1f) &&
                collision.gameObject.transform.position.x <= (this.gameObject.transform.position.x - 0.5f))
            {
                anim.Play("idleiz");
            }
            else if (collision.gameObject.transform.position.y < (this.gameObject.transform.position.x + 0.1f) &&
                collision.gameObject.transform.position.x <= (this.gameObject.transform.position.x + 0.5f) &&
                collision.gameObject.transform.position.x > (this.gameObject.transform.position.x))
            {
                anim.Play("idlede");
            }
            else
            {
                anim.Play("idlear");
            }
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sortingOrder = 1;
            if (collision.gameObject.transform.position.y < (this.gameObject.transform.position.x + 0.1f) &&
                collision.gameObject.transform.position.x <= (this.gameObject.transform.position.x - 0.5f))
            {
                anim.Play("idleiz");
            }
            else if (collision.gameObject.transform.position.y < (this.gameObject.transform.position.x + 0.1f) &&
                collision.gameObject.transform.position.x <= (this.gameObject.transform.position.x + 0.5f) &&
                collision.gameObject.transform.position.x > (this.gameObject.transform.position.x))
            {
                anim.Play("idlede");
            }
            else
            {
                anim.Play("idleab");
            }
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.transform.position.y > this.gameObject.transform.position.y)
        {
            this.GetComponent<SpriteRenderer>().sortingOrder = 2;
            if (collision.gameObject.transform.position.y < (this.gameObject.transform.position.x + 0.1f) &&
                collision.gameObject.transform.position.x <= (this.gameObject.transform.position.x - 0.5f))
            {
                anim.Play("idleiz");
            }
            else if (collision.gameObject.transform.position.y < (this.gameObject.transform.position.x + 0.1f) &&
                collision.gameObject.transform.position.x <= (this.gameObject.transform.position.x + 0.5f) &&
                collision.gameObject.transform.position.x > (this.gameObject.transform.position.x))
            {
                anim.Play("idlede");
            }
            else
            {
                anim.Play("idlear");
            }
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sortingOrder = 1;
            if (collision.gameObject.transform.position.y < (this.gameObject.transform.position.x + 0.1f) &&
                collision.gameObject.transform.position.x <= (this.gameObject.transform.position.x - 0.5f))
            {
                anim.Play("idleiz");
            }
            else if (collision.gameObject.transform.position.y < (this.gameObject.transform.position.x + 0.1f) &&
                collision.gameObject.transform.position.x <= (this.gameObject.transform.position.x + 0.5f) &&
                collision.gameObject.transform.position.x > (this.gameObject.transform.position.x))
            {
                anim.Play("idlede");
            }
            else
            {
                anim.Play("idleab");
            }
        }
    }
}
