using OdinSerializer;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{
    public SaveSO save;
    public GameObject canvas;
    static GameObject thisSM = null;
    void Awake()
    {
        if (thisSM == null)
        {
            thisSM = this.gameObject;
            DontDestroyOnLoad(thisSM);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    void Start()
    {
        canvas.GetComponent<MenuInevtory>().saveGame += saveGame;
        /*if(save.IsCharged)
        {
            string newBase64 = File.ReadAllText("save.json");
            byte[] postFile = System.Convert.FromBase64String(newBase64);
            //byte[] postFile = File.ReadAllBytes("testOdin.json");


            print(postFile);


            SaveSO newData = SerializationUtility.DeserializeValue<SaveSO>(postFile, DataFormat.JSON);

            save.inventory = newData.inventory;
            save.party = newData.party;
            save.prota1 = newData.prota1;
            save.prota2 = newData.prota2;
            save.quests.Clear();
            foreach(QuestSO q in newData.quests)
            {
                save.quests.Add(q);
            }
        }*/
    }

    public void saveGame()
    {
        byte[] serializedData = SerializationUtility.SerializeValue<SaveSO>(save, DataFormat.JSON);
        string base64 = System.Convert.ToBase64String(serializedData);
        print("saving...");
        File.WriteAllText("save.json", base64);
    }
    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.K))
        {
            saveGame();
            //A clone = Instantiate(newData);
        }

        else if (Input.GetKeyDown(KeyCode.L))
        {
            string newBase64 = File.ReadAllText("save.json");
            byte[] postFile = System.Convert.FromBase64String(newBase64);
            //byte[] postFile = File.ReadAllBytes("testOdin.json");


            print(postFile);


            SaveSO newData = SerializationUtility.DeserializeValue<SaveSO>(postFile, DataFormat.JSON);
            print("loading...");
            save.party.money = newData.party.money;
            foreach(PlayableCharacters pc in newData.party.party)
            {
                PlayableCharacters a = ScriptableObject.CreateInstance<PlayableCharacters>();
                a = pc;
                save.party.party.Add(a);
            }
            //save.party.party = newData.party.party;
            save.party.inventory.fullClear();
            //Despu�s del fullClear del inventario, iteramos por los inventarios almacenados en newData
            //y para uno de los objetos, generamos un nuevo SO del tipo en cuesti�n y lo almacenamos
            //en el inventario de nuestro save asignando los valores de los campos correspondientes...
            foreach(RPGItem c in newData.inventory.Consumables)
            {
                if(c is Potion)
                {
                    Potion ca = (Potion)c;
                    Potion a = ScriptableObject.CreateInstance<Potion>();
                    a.name = ca.name;
                    a.description = ca.description;
                    a.sprite = ca.sprite;
                    a.type = ca.type;
                    a.m_affectedStat = ca.m_affectedStat;
                    a.m_buyingPrice = ca.m_buyingPrice;
                    a.m_sellingPrice = ca.m_sellingPrice;
                    a.m_ges = ca.m_ges;
                    a.m_geiResta = ca.m_geiResta;
                    a.m_geiSuma = ca.m_geiSuma;
                    save.party.inventory.Consumables.Add((RPGItem)a);
                }
                
            }
            foreach (RPGItem e in newData.inventory.Equipment)
            {
                if(e is Armor)
                {
                    Armor ea = (Armor)e;
                    Armor a = ScriptableObject.CreateInstance<Armor>();
                    a.sprite = ea.sprite;
                    a.description = ea.description;
                    a.type = ea.type;
                    a.m_defense = ea.m_defense;
                    a.m_buyingPrice = ea.m_buyingPrice;
                    a.m_sellingPrice = ea.m_sellingPrice;
                    save.party.inventory.Equipment.Add((RPGItem)a);
                }
                else if(e is Weapon)
                {
                    Weapon ea = (Weapon)e;
                    Weapon a = ScriptableObject.CreateInstance<Weapon>();
                    a.sprite = e.sprite;
                    a.description = e.description;
                    a.type = e.type;
                    a.m_weaponType = ea.m_weaponType;
                    a.geiAdd = ea.geiAdd;
                    a.geiRemove = ea.geiRemove;
                    a.m_dexterityLvl = ea.m_dexterityLvl;
                    a.m_buyingPrice = ea.m_buyingPrice;
                    a.m_sellingPrice = ea.m_sellingPrice;
                    save.party.inventory.Equipment.Add((RPGItem)a);
                }
                
            }
           // save.prota1 = newData.prota1;
           // save.prota2 = newData.prota2;

        }
    }
}
