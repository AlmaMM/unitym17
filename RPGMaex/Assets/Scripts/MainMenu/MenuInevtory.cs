using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static Movement;

public class MenuInevtory : MonoBehaviour
{
    GameObject protaFade;
    GameObject Pointer;
    GameObject EquipmentMenu;
    GameObject ConsumMenu;
    bool bat = false;
    GameObject SubmenuPointer;
    GameObject infoTxt;
    public delegate void saveGameHandler();
    public event saveGameHandler saveGame;
    // Start is called before the first frame update
    void Start()
    {
        EquipmentMenu = GameObject.Find("Canvas").transform.GetChild(6).Find("EquipmentMen�").gameObject;
        ConsumMenu = GameObject.Find("Canvas").transform.GetChild(6).Find("ConsumablesMen�").gameObject;
        Pointer = GameObject.Find("Canvas").transform.GetChild(6).transform.GetChild(2).gameObject; 
        infoTxt = GameObject.Find("Canvas").transform.GetChild(6).Find("infoPanel").gameObject;
        SubmenuPointer = GameObject.Find("Canvas").transform.GetChild(6).Find("PointerSubmenu").gameObject;
        protaFade = GameObject.Find("Prota");
        protaFade.GetComponent<Movement>().mFadeCanvas += goFade;


    }

    public void goFade(int i)
    {
        switch(i)
        {
            case 3:
                StartCoroutine(fade3(this.transform.Find("Presentacion").GetComponent<Image>()));
                StartCoroutine(fade3(this.transform.Find("Title").GetComponent<Image>()));
                StartCoroutine(fade3(this.transform.Find("Logo").GetComponent<Image>()));
                break;
            case 1:
                StartCoroutine(fade(this.GetComponentInChildren<Image>()));
                break;
            case 2:
                StartCoroutine(fade2(this.GetComponentInChildren<Image>()));
                break;
        }
            
    }

    IEnumerator fade(Image t)
    {

        for (int i = 0; i < 55; i++)
        {
            yield return new WaitForSeconds(.01f);
            t.GetComponent<Image>().color = new Color(t.GetComponent<Image>().color.r,
                t.GetComponent<Image>().color.g,
                t.GetComponent<Image>().color.b,
                t.GetComponent<Image>().color.a + 0.02f);
        }

    }

    IEnumerator fade2(Image t)
    {
        t.GetComponent<Image>().color = new Color(t.GetComponent<Image>().color.r,
                t.GetComponent<Image>().color.g,
                t.GetComponent<Image>().color.b, 1);

        yield return new WaitForSeconds(1);

        for (int i = 0; i < 55; i++)
        {
            yield return new WaitForSeconds(.01f);
            t.GetComponent<Image>().color = new Color(t.GetComponent<Image>().color.r,
                t.GetComponent<Image>().color.g,
                t.GetComponent<Image>().color.b,
                t.GetComponent<Image>().color.a - 0.02f);
        }

    }

    IEnumerator fade3(Image t)
    {
        t.GetComponent<Image>().color = new Color(t.GetComponent<Image>().color.r,
                t.GetComponent<Image>().color.g,
                t.GetComponent<Image>().color.b, 1);

        yield return new WaitForSeconds(0.7f);

        for (int i = 0; i < 200; i++)
        {
            yield return new WaitForSeconds(.02f);
            t.GetComponent<Image>().color = new Color(t.GetComponent<Image>().color.r,
                t.GetComponent<Image>().color.g,
                t.GetComponent<Image>().color.b,
                t.GetComponent<Image>().color.a - 0.01f);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    static GameObject canvasObject = null;



    void Awake()
    {
        if (canvasObject == null)
        {
            canvasObject = this.gameObject;
            SceneManager.sceneLoaded += onSceneLoaded;
            DontDestroyOnLoad(canvasObject);
            DontDestroyOnLoad(EventSystem.current);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void onSceneLoaded(Scene s, LoadSceneMode lsm)
    {
        if (s.name == "Combate")
        {
            bat = true;
        }
        else
        {
            bat = false;
        }
    }

    public delegate void enableOffMovement(bool b);
    public event enableOffMovement onOffMovement;


    public void abrirMenu(InputAction.CallbackContext context)
    {
        if (context.performed && !bat)
        {
            onOffMovement?.Invoke(false);
            if (!this.transform.Find("1").gameObject.activeSelf)
            {
                this.transform.Find("1").gameObject.SetActive(true);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(this.transform.GetChild(4).transform.GetChild(0).gameObject);
            }
        }
    }

    public void cerrarMenu(InputAction.CallbackContext context)
    {
        if (context.performed && !bat)
        {
            if (this.transform.Find("1").gameObject.activeSelf)
            {
                if (this.transform.Find("menuStats").gameObject.activeSelf || this.transform.Find("menuInventory").gameObject.activeSelf ||
                    this.transform.Find("menuGuardar").gameObject.activeSelf)
                {

                    if (this.transform.Find("menuInventory").gameObject.activeSelf)
                    {
                        if (infoTxt.activeSelf)
                        {
                            infoTxt.SetActive(false);
                        }
                        if ((ConsumMenu.activeSelf || EquipmentMenu.activeSelf))
                        {
                            Pointer.SetActive(true);
                            ConsumMenu.SetActive(false);
                            EquipmentMenu.SetActive(false);
                            SubmenuPointer.SetActive(false);
                            EventSystem.current.SetSelectedGameObject(null);
                        }
                        /*else if(SubmenuPointer.activeSelf)
                        {
                            EventSystem.current.SetSelectedGameObject(null);
                            EventSystem.current.SetSelectedGameObject(this.transform.GetChild(6).transform.GetChild(0).gameObject);

                            Pointer.SetActive(true);
                        }*/
                        else if (Pointer.activeSelf)
                        {
                            EventSystem.current.SetSelectedGameObject(null);
                            EventSystem.current.SetSelectedGameObject(this.transform.GetChild(6).transform.GetChild(0).gameObject);
                            Pointer.SetActive(false);

                        }
                        else
                        {
                            EventSystem.current.SetSelectedGameObject(null);
                            EventSystem.current.SetSelectedGameObject(this.transform.GetChild(4).transform.GetChild(1).gameObject);
                            this.transform.Find("menuStats").gameObject.SetActive(false);
                            this.transform.Find("menuInventory").gameObject.SetActive(false);
                            this.transform.Find("menuGuardar").gameObject.SetActive(false);
                        }

                    }
                    if (this.transform.Find("menuStats").gameObject.activeSelf)
                    {
                        EventSystem.current.SetSelectedGameObject(null);
                        EventSystem.current.SetSelectedGameObject(this.transform.GetChild(4).transform.GetChild(0).gameObject);
                        this.transform.Find("menuStats").gameObject.SetActive(false);
                    }

                }
                else
                {
                    this.transform.Find("1").gameObject.SetActive(false);
                    onOffMovement?.Invoke(true);
                    EventSystem.current.SetSelectedGameObject(null);
                }
            }

            
        }
    }

    public void menuInventory(int v)
    {
        switch (v)
        {
            case 0:
                this.transform.Find("menuStats").gameObject.SetActive(true);
                this.transform.Find("menuInventory").gameObject.SetActive(false);
                this.transform.Find("menuGuardar").gameObject.SetActive(false);
                break;
            case 1:
                this.transform.Find("menuInventory").gameObject.SetActive(true);
                this.transform.Find("menuInventory").transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(true);
                this.transform.Find("menuInventory").transform.GetChild(1).transform.GetChild(1).transform.GetChild(0).gameObject.SetActive(true);
                this.transform.Find("menuStats").gameObject.SetActive(false);
                this.transform.Find("menuGuardar").gameObject.SetActive(false);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(this.transform.GetChild(6).transform.GetChild(0).gameObject);
                break;
            case 2:
                this.transform.Find("menuGuardar").gameObject.SetActive(true);
                this.transform.Find("menuInventory").gameObject.SetActive(false);
                this.transform.Find("menuStats").gameObject.SetActive(false);
                this.transform.Find("1").gameObject.SetActive(false);
                this.transform.GetChild(7).transform.GetChild(0).gameObject.SetActive(true);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(this.transform.GetChild(7).transform.GetChild(0).transform.GetChild(1).gameObject);
                break;
            case 3:
                this.transform.GetChild(7).transform.GetChild(0).gameObject.SetActive(false);
                this.transform.GetChild(7).transform.GetChild(1).gameObject.SetActive(true);
                StartCoroutine(guardarYCerrarMenu());
                break;
            case 4:
                this.transform.GetChild(7).gameObject.SetActive(false);
                this.transform.Find("1").gameObject.SetActive(true);
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(this.transform.GetChild(4).transform.GetChild(2).gameObject);
                break;
            case 5:
                EventSystem.current.SetSelectedGameObject(null);          
                break;

        }
    }

    IEnumerator guardarYCerrarMenu()
    {
        saveGame.Invoke();
        yield return new WaitForSeconds(2);
        this.transform.GetChild(7).transform.GetChild(0).gameObject.SetActive(false);
        this.transform.GetChild(7).transform.GetChild(1).gameObject.SetActive(false);
        this.transform.Find("menuGuardar").gameObject.SetActive(false);
        this.transform.Find("menuInventory").gameObject.SetActive(false);
        this.transform.Find("menuStats").gameObject.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        onOffMovement?.Invoke(true);

        //crear funcion guardar o llamarla donde quiera que este
    }

}
