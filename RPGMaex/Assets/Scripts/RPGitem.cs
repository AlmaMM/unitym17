using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[Serializable]
public class RPGItem : ScriptableObject
{
    public string name;
    public Nature type;
    public Sprite sprite;
    public string description;
}
public interface Equipable
{
    void equip(PlayableCharacters c);
    void unequip();
}

public interface Consumable
{
    void consume(Characters c);
}

public interface Buyable
{
    public float BuyingPrice { get; }
    public float SellingPrice { get; }

    void buy();
    void sell();

}
public enum Nature
{
    MYSTICAL,PHYSICAL,PSYCHIC,NONE
}

public enum Stats
{ 
    HP,ATTK,DEF,PP,SPD,ALL
}

