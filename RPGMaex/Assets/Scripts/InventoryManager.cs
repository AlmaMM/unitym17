using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class InventoryManager : ScriptableObject
{
    public int MAX_INVENTORY;
    public List<RPGItem> Equipment = new List<RPGItem>();
    public List<RPGItem> Consumables = new List<RPGItem>();
    public void addItem(RPGItem item)
    {
        if (item is Equipable)
        {
            Debug.Log("(equipable) ESTOY A�ADIENDO: " + item);
            if (Equipment.Count < MAX_INVENTORY)
            {
                Equipment.Add(item);
                Debug.Log("HE A�ADIDO: " + item);
            }
            else
            {
                //TODO FULL INVENTORY -> Show message.
                Debug.Log("INVENTARIO COMPLETO. POR FAVOR PARA. UF.");
            }
        }else if (item is Consumable)
        {
            Debug.Log("(consumable) ESTOY A�ADIENDO: " + item);
            if (Consumables.Count < MAX_INVENTORY)
            {
                Consumables.Add(item);
                Debug.Log("HE A�ADIDO: " + item);
            }
            else
            {
                //TODO FULL INVENTORY -> Show message.
                Debug.Log("INVENTARIO COMPLETO. POR FAVOR PARA. UF.");
            }
        }

    }

    public void removeItem(RPGItem item)
    {
        Debug.Log("REMOVING");
        if (item is Equipable)
        {
            Debug.Log("ESTOY ELIMINANDO: " + item);
            if (Equipment.Contains( item))
            {
                Equipment.Remove(item);
                Debug.Log("HE ELIMINADO: " + item);
            }
        }
        else if (item is Consumable)
        {
            Debug.Log("ESTOY ELIMINANDO: " + item);
            if (Consumables.Contains(item))
            {
                Consumables.Remove(item);
                Debug.Log("HE ELIMINADO: " + item);
            }
        }
    }

    public List<RPGItem> getEquipment()
    {
        return this.Equipment;
    }

    public List<RPGItem> getConsumables()
    {
        return this.Consumables;
    }

    public List<RPGItem> getInventory()
    {
        List<RPGItem> eq = new List<RPGItem>(getEquipment());
        eq.AddRange(getConsumables());
        return eq;
    }

    public void fullClear()
    {
        Consumables.Clear();
        Equipment.Clear();
    }




}
