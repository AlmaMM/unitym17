using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayableCharacters : Characters
{
    public WeaponTypes[] m_WeaponsAffinity = new WeaponTypes[3];
    public Weapon m_equipedWeapon;
    public Armor m_equipedArmor;
    private int m_maxExp = 100;

    public float getCurrentExp()
    {
        return m_exp;
    }

    public void setCurrentExp(float exp)
    {
        this.m_exp += exp;
        while (m_exp >= m_maxExp)
        {
            this.m_exp = m_exp - m_maxExp;
            this.m_lvl++;
        }
    }

    public float getAttk()
    {
        
        return m_ATTK;
    }
    public float getHp()
    {
        //TODO
        return m_CurrentHP;
    }
    public float getDef()
    {
        //TODO
        return m_DEF;
    }

    public float getSpd()
    {
        //TODO
        return m_SPD;
    }

    public void takeDamage()
    {
        //TODO
    }


}
