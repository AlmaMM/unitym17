using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UpdateTxt : MonoBehaviour
{
    public GameObject atq1, atq2, def1, def2, vel1, vel2, hp1, hp2, pp1, pp2, nombre1, nvl1, nvl2, exp1, exp2, monedas, timePlayed, 
        panelVida1, panelVida2, panelPP1, panelPP2, panelNombre1, textBox;
    public Characters stats1, stats2;

    GameObject frase1, frase2, escenaActiva;

    Scene scene;
    LoadSceneMode lsm;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(timeCo());

        escenaActiva = GameObject.Find("Prota");
        escenaActiva.GetComponent<Movement>().mEscenaActiva += activarBusquedaObjecto;
    }


    public void mostrarTexto(string s)
    {
        textBox.SetActive(true);
        textBox.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "NPC: "+s;
        StartCoroutine(setActiveFalse());
    }



    public void activarBusquedaObjecto(int i)
    {
        if (i == 1)
        {
            frase1 = GameObject.Find("PreFabCh");
            frase1.GetComponent<Aldeano>().mostrarTextoDelegator += mostrarTexto;
        }
        else if(i == 2)
        {
            frase2 = GameObject.Find("PreFabCh1");
            frase2.GetComponent<Aldeano>().mostrarTextoDelegator += mostrarTexto;
        }
        
    }

    

    IEnumerator setActiveFalse()
    {
        yield return new WaitForSeconds(3);
        textBox.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
        atq1.GetComponent<TextMeshProUGUI>().text = "+ "+ Mathf.Round(stats1.m_ATTK);
        def1.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(stats1.m_DEF);
        vel1.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(stats1.m_SPD);
        hp1.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(stats1.m_MaxHP);
        pp1.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(stats1.m_MaxPP);
        exp1.GetComponent<TextMeshProUGUI>().text = "EXP. " + Mathf.Round(stats1.m_exp)+"/100";
        nombre1.GetComponent<TextMeshProUGUI>().text = stats1.m_name;
        nvl1.GetComponent<TextMeshProUGUI>().text = "LVL. " + stats1.m_lvl;

        panelNombre1.GetComponent<TextMeshProUGUI>().text = stats1.m_name;
        panelVida1.GetComponent<TextMeshProUGUI>().text = "HP\n"+Mathf.Round(stats1.m_CurrentHP) + "/" + Mathf.Round(stats1.m_MaxHP);
        panelPP1.GetComponent<TextMeshProUGUI>().text = "PP\n"+Mathf.Round(stats1.m_CurrentPP) + "/" + Mathf.Round(stats1.m_MaxPP);

        atq2.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(stats2.m_ATTK);
        def2.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(stats2.m_DEF);
        vel2.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(stats2.m_SPD);
        hp2.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(stats2.m_MaxHP);
        pp2.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(stats2.m_MaxPP);
        exp2.GetComponent<TextMeshProUGUI>().text = "EXP. " + Mathf.Round(stats2.m_exp) + "/100";
        nvl2.GetComponent<TextMeshProUGUI>().text = "LVL. " + stats2.m_lvl;

        panelVida2.GetComponent<TextMeshProUGUI>().text = "HP\n" + Mathf.Round(stats2.m_CurrentHP) + "/" + Mathf.Round(stats2.m_MaxHP);
        panelPP2.GetComponent<TextMeshProUGUI>().text = "PP\n" + Mathf.Round(stats2.m_CurrentPP) + "/" + Mathf.Round(stats2.m_MaxPP);
    }

    IEnumerator timeCo()
    {
        int s = 0;
        int m = 00;
        int h = 00;
        while(true)
        {
            if(s>59)
            {
                s = 0;
                m++;
                if(m>59)
                {
                    h++;
                }
            }
            yield return new WaitForSeconds(1);

            if (m < 10)
            {
                if(s<10)
                {
                    timePlayed.GetComponent<TextMeshProUGUI>().text = "Time played: 0" + h + ":0" + m + ":0" + s;
                }
                else
                {
                    timePlayed.GetComponent<TextMeshProUGUI>().text = "Time played: 0" + h + ":0" + m + ":" + s;
                }

            }
            else
            {
                if (s < 10)
                {
                    timePlayed.GetComponent<TextMeshProUGUI>().text = "Time played: 0" + h + ":" + m + ":0" + s;
                }
                else
                {
                    timePlayed.GetComponent<TextMeshProUGUI>().text = "Time played: 0" + h + ":" + m + ":" + s;
                }
            }
            s++;
        }
    }

}
