using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    static GameObject thisQM = null;
    public List<QuestSO> quests;

    void Awake()
    {
        if (thisQM == null)
        {
            thisQM = this.gameObject;
            DontDestroyOnLoad(thisQM);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
