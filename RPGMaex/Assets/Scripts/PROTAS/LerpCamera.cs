using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class LerpCamera : MonoBehaviour
{
    public ScpObjTrnsPro prota;
    Transform target;
    static bool b = true;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LerpPositionAmbSlowDown());
    }

    IEnumerator LerpPositionAmbSlowDown()
    {
        float time = 0;

        yield return new WaitForSeconds(0.2f);
        target = prota.protaTransform;

        if(b)
        {
            yield return new WaitForSeconds(1);
            b = false;
        }
        while (true)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x, target.position.y, -10), (Time.deltaTime*3f));
            time += (Time.deltaTime * 3f);
            yield return null;
        }
    }
}
