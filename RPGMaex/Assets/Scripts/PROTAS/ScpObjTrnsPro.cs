using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScpObjTrnsPro : ScriptableObject
{
    public Transform protaTransform;
}
