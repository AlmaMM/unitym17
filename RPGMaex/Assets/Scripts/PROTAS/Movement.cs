using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEditor.UI;
using JetBrains.Annotations;
using Random = UnityEngine.Random;

public class Movement : MonoBehaviour
{
    public Sprite arriba, abajo, izquierda, derecha;
    public Animator anim;
    private int spd = 3;
    bool ar, ab, de, iz, ow;
    Vector2 v2;
    public ScpObjTrnsPro p;
    Vector3 p2, p3;
    bool canMove = true;
    bool hablarNPC;

    int timeToFight = 1000;
    bool restarTimeToFight, bat;

    public Characters prota1;

    GameObject delegadoMenu;

    static GameObject instantiateProta = null;
    static bool unaVez;

    public delegate void fadeCanvas(int i);
    public event fadeCanvas mFadeCanvas;

    public delegate void getEscenaActiva(int i);
    public event getEscenaActiva mEscenaActiva;

    GameObject aldeano;
    


    void Awake()
    {
        if (instantiateProta == null)
        {
            instantiateProta = this.gameObject;
            SceneManager.sceneLoaded += onSceneLoaded;
            DontDestroyOnLoad(instantiateProta);
        }
        else
        {
            Destroy(this.gameObject);
        }

        prota1.m_ATTK = 15;
        prota1.m_DEF = 10;
        prota1.m_SPD = 5;
        prota1.m_MaxHP = 100;
        prota1.m_CurrentHP = 99;
        prota1.m_MaxPP = 24;
        prota1.m_CurrentPP = 20;
        prota1.m_lvl = 1;
        prota1.m_name = "awd";
        prota1.m_exp = 0;
    }

    void onSceneLoaded(Scene s, LoadSceneMode lsm)
    {
        restarTimeToFight = false;
        canMove = true;
        if (s.name == "VillageCasa1")
        {
            mEscenaActiva.Invoke(1);
            instantiateProta.GetComponent<Rigidbody2D>().transform.position = new Vector3(0, -2.5f, 1);
            mFadeCanvas?.Invoke(2);
        }
        else if (s.name == "Pueblo")
        {
            mEscenaActiva.Invoke(1);
            this.transform.GetComponent<UnityEngine.Rendering.Universal.Light2D>().enabled = false;
            
            if(ow)
            {
                instantiateProta.transform.position = new Vector3(p2.x - 0.5f , p2.y, 1);
            }
            else
            {
                ow = false;
                instantiateProta.transform.position = new Vector3(p2.x, p2.y - 0.5f, 1);
            }

            mFadeCanvas?.Invoke(2);

            if(!unaVez)
            {
                instantiateProta.GetComponent<Rigidbody2D>().transform.position = new Vector3(-8.71f, -5.12f, 1);
                unaVez = true;
            }
                
        }
        else if (s.name == "VillageCasa2")
        {
            mEscenaActiva.Invoke(1);
            instantiateProta.GetComponent<Rigidbody2D>().transform.position = new Vector3(3, -2.5f, 1);
            mFadeCanvas?.Invoke(2);
        }
        else if (s.name == "Cave")
        {
            //mEscenaActiva.Invoke();
            this.transform.GetComponent<UnityEngine.Rendering.Universal.Light2D>().enabled = true;
            instantiateProta.GetComponent<Rigidbody2D>().transform.position = new Vector3(0, -.5f, 1);
            mFadeCanvas?.Invoke(2);
        }
        else if (s.name == "OutWorld")
        {
            mEscenaActiva.Invoke(1);
            mEscenaActiva.Invoke(2);
            ow = true;
            if (bat)
            {
                this.GetComponent<SpriteRenderer>().enabled = true;
                instantiateProta.transform.position = new Vector3(p3.x, p3.y, 1);
                this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                animatorCancel();
                bat = false;
            }
            else
            {
                instantiateProta.GetComponent<Rigidbody2D>().transform.position = new Vector3(1, 0, 1);
            }
            mFadeCanvas?.Invoke(2);

            timeToFight = Random.Range(20, 150);
            restarTimeToFight = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        canMove = false;
        StartCoroutine(Presentacion());

        delegadoMenu = GameObject.Find("Canvas");
        delegadoMenu.GetComponent<MenuInevtory>().onOffMovement += setearScript;
    }

    public void setearScript(bool b)
    {
        canMove = b;
        animatorCancel();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "VillagePuertaCasa1")
        {
            ow = false;
            canMove = false;
            animatorCancel();
            p2 = this.GetComponent<Transform>().position;
            mFadeCanvas?.Invoke(1);
            StartCoroutine(LoadMap(2, 0.55f));
        }
        else if(collision.transform.tag == "VillagePuertaCasa1Dentro")
        {
            canMove = false;
            animatorCancel();
            mFadeCanvas?.Invoke(1);
            StartCoroutine(LoadMap(0, 0.55f));
        }
        else if (collision.transform.tag == "VillagePuertaCasa2")
        {
            ow = false;
            canMove = false;
            animatorCancel();
            p2 = this.GetComponent<Transform>().position;
            mFadeCanvas?.Invoke(1);
            StartCoroutine(LoadMap(3, 0.55f));
        }
        else if (collision.transform.tag == "VillagePuertaCueva")
        {
            ow = false;
            canMove = false;
            animatorCancel();
            p2 = this.GetComponent<Transform>().position;
            mFadeCanvas?.Invoke(1);
            StartCoroutine(LoadMap(4, 0.55f));
        }
        else if (collision.transform.tag == "VillagePuertaVillage")
        {
            canMove = false;
            animatorCancel();
            p2 = this.GetComponent<Transform>().position;
            mFadeCanvas?.Invoke(1);
            StartCoroutine(LoadMap(5, 0.55f));
        }
        else if(collision.transform.tag == "Cofre")
        {
            collision.gameObject.GetComponent<Cofre>().open();
        }
        else if(collision.transform.tag == "NPC")
        {
            hablarNPC = true;
            aldeano = collision.gameObject;
        }

    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "NPC")
        {
            hablarNPC = false;
        }
    }

    public void btnAccion(InputAction.CallbackContext c)
    {
        if (c.performed && hablarNPC)
        {
            aldeano.gameObject.GetComponent<Aldeano>().accionar();
        }
    }

    IEnumerator LoadMap(int i, float j)
    {
        yield return new WaitForSeconds(j);
        SceneManager.LoadScene(i);
        canMove = true;
    }

    private void Update()
    {
        p.protaTransform = this.GetComponent<Rigidbody2D>().transform;
    }

    public void Mover(InputAction.CallbackContext c)
    {
        if(canMove && !bat)
        {
            if(restarTimeToFight)
            {
                timeToFight--;

                if(timeToFight < 1)
                {
                    p3 = this.GetComponent<Transform>().position;
                    bat = true;
                    this.GetComponent<SpriteRenderer>().enabled = false;
                    SceneManager.LoadScene(7);
                }
            }
            v2 = c.ReadValue<Vector2>();
            if (c.performed)
            {
                anim.enabled = true;
                if (Math.Abs(v2.x) > Math.Abs((v2.y)))
                {
                    v2.y = 0;
                }
                else
                {
                    v2.x = 0;
                }
                daleZelda();
                if (v2.x > 0 && v2.y < 0.5f && v2.y > -0.5f)
                {
                    anim.Play("CaminarDerecha");
                    ar = false;
                    ab = false;
                    iz = false;
                    de = true;
                }
                else if (v2.x < 0)
                {
                    anim.Play("CaminarIzquierda");
                    ar = false;
                    ab = false;
                    iz = true;
                    de = false;
                }
                else if (v2.y > 0.5f)
                {
                    anim.Play("CaminarArriba");
                    ar = true;
                    ab = false;
                    iz = false;
                    de = false;
                }
                else if (v2.y < -0.5f)
                {
                    anim.Play("CaminarAbajo");
                    ar = false;
                    ab = true;
                    iz = false;
                    de = false;
                }
            }
            if (c.canceled)
            {
                animatorCancel();
            }
        }
    }

    private void animatorCancel()
    {
        anim.enabled = false;
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        if (ar)
        {
            this.GetComponent<SpriteRenderer>().sprite = arriba;
        }
        else if (ab)
        {
            this.GetComponent<SpriteRenderer>().sprite = abajo;
        }
        else if (de)
        {
            this.GetComponent<SpriteRenderer>().sprite = derecha;
        }
        else if (iz)
        {
            this.GetComponent<SpriteRenderer>().sprite = izquierda;
        }
    }

    public void correr(InputAction.CallbackContext c)
    {
        int con = 0;
        if(c.performed)
        {
            spd = 5;
            if(con==0 && canMove)
            {
                con++;
                daleZelda();
            }
            
        }
        else if (c.canceled && canMove)
        {
            spd = 3;
            if (con == 0)
            {
                con++;
                daleZelda();
            }
        }
    }

    public void daleZelda()
    {
        this.GetComponent<Rigidbody2D>().velocity = v2 * spd;
    }

    IEnumerator Presentacion()
    {
        yield return new WaitForSeconds(0.2f);
        mFadeCanvas?.Invoke(3);

        yield return new WaitForSeconds(2.6f);
        canMove = true;
    }

    
}
