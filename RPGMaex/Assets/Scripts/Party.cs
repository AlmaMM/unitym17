using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class Party : ScriptableObject
{
    public List<PlayableCharacters> party;
    public InventoryManager inventory;
    public float money;

    public void setMoney(float money)
    {
        this.money += money;
    }

}
