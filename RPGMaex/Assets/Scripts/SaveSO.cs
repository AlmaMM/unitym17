using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SaveSO : ScriptableObject
{
    public Party party;
    public PlayableCharacters prota1;
    public PlayableCharacters prota2;
    public InventoryManager inventory;
    public List<QuestSO> quests;
    private bool isCharged;
    public bool IsCharged => isCharged;


}
