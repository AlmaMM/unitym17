using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cofre : MonoBehaviour
{
    public NonPlayableCharacters npc;
    public Animator anim;
    public Sprite closed, opened;
    public bool isStart = true;

    void Start()
    {
        if(isStart)
        {
            npc.haveItem = true;
        }
        isStart = false;
        anim.enabled = false;
        if (!npc.haveItem)
        {
            this.GetComponent<SpriteRenderer>().sprite = opened;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = closed;
        }
    }

    public void open()
    {
        StartCoroutine(opening());
    }

    IEnumerator opening()
    {
        if(npc.haveItem)
        {
            this.npc.haveItem = false;
            anim.enabled = true;
            anim.Play("CofreOpening");
            yield return new WaitForSeconds(0.5f);
            anim.enabled = false;
            this.GetComponent<SpriteRenderer>().sprite = opened;
            npc.give();
        }

    }

}
