using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Experimental.GraphView;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public tipusEnemic[] tipusEnemic;
    public GameObject enemy;
    public DadesPersistents data;
    bool isActive = true;
    IEnumerator lasercr;
    int lasercd = 60;
    public TMPro.TextMeshProUGUI countdownTxt;
    public Button laser;
    public List<GameObject> enemyPool;
    public float spawnCD;
    // Start is called before the first frame update
    void Start()
    {
        //countdownTxt = GameObject.Find("Countdown").GetComponent<TMPro.TextMeshProUGUI>();
        

        foreach (tipusEnemic e in tipusEnemic)
        {
            for (int i = 0; i < 50; i++)
            {
                GameObject newEnemy = Instantiate(enemy);
                newEnemy.GetComponent<SpriteRenderer>().color = e.color;
                newEnemy.GetComponent<Enemy>().spd = e.velocitat;
                newEnemy.GetComponent<Enemy>().damage = e.damage;

                newEnemy.SetActive(false);
                enemyPool.Add(newEnemy);

            }
        }
        StartCoroutine(EnemySpawner());
        lasercr = LaserCD();
        StartCoroutine(lasercr);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void win()
    {
        data.tempsTranscorregut = Time.captureDeltaTime;
        SceneManager.LoadScene("win");
    }


    IEnumerator EnemySpawner()
    {
        while (true)
        {
            float delay;
            if (data.canvisEscena == 0)
            {
                delay = 1;
            }
            else
            {
                delay = data.canvisEscena * 0.1f;
            }
            yield return new WaitForSeconds(spawnCD*delay);
            int r = Random.Range(0, enemyPool.Count);
            if (!enemyPool[r].activeSelf)
            {
                enemyPool[r].SetActive(true);
            }
            
            
        }
    }


    IEnumerator LaserCD()
    {
        while (isActive)
        {
            if (lasercd <= 0)
            {
                laser.gameObject.SetActive(true);
                isActive = false;
            }
            else
            {
                countdownTxt.text = "Countdown: " + lasercd;
                lasercd--;
                yield return new WaitForSeconds(1);
            }
            
        }
    }
}
