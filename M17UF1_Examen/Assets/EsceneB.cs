using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

public class EsceneB : MonoBehaviour
{
    public GameObject carregaPrefab;
    public DadesPersistents data;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnShield());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator spawnShield()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            GameObject newCarrega = Instantiate(carregaPrefab);
            newCarrega.transform.position = new Vector3(-10, 0,0);
            int r = Random.Range(0, 2);
            if (r == 0)
            {
                newCarrega.GetComponent<SpriteRenderer>().color = Color.red;
            }
            else
            {
                newCarrega.GetComponent<SpriteRenderer>().color = Color.green;
            }
            
            newCarrega.GetComponent<Rigidbody2D>().AddForce(Vector2.right*2, ForceMode2D.Impulse);
            data.force = (Vector2.right * 2).magnitude;
        }
    }
}
