using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    GameObject player;
    GameObject escut;
    public int spd;
    public int damage;
    void Start()
    {

        escut = GameObject.Find("estrella").transform.GetChild(1).gameObject;
        player = GameObject.Find("estrella").transform.GetChild(0).gameObject;
        //Implementació del patró obersver amb delegats
        /*player = GameObject.Find("Player");
        player.GetComponent<Player>().saltEvent += saysOw;*/
    }
    private void OnEnable()
    {
        int r = Random.Range(0, 4);
        switch(r)
        {
            case 0:
                this.transform.position = new Vector2(-10, Random.Range(-5, 5));
                break;

            case 1:
                this.transform.position = new Vector2(10, Random.Range(-5, 5));
                break;

            case 2:
                this.transform.position = new Vector2(Random.Range(-10, 10), -5);
                break;

            case 3:
                this.transform.position = new Vector2(Random.Range(-10, 10), 5);
                break;
        }
        
        Vector2 direction = Vector3.zero - this.transform.position;
        direction.Normalize();
        this.GetComponent<Rigidbody2D>().velocity =  direction*spd;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == escut)
        {
            this.gameObject.SetActive(false);
            if (escut.GetComponentInParent<Player>().vidaEscut > 0)
            {
                escut.GetComponentInParent<Player>().vidaEscut-=this.damage;

            }
            else
            {
                SceneManager.LoadScene("EsceneB");
            }
            
        }
        if(collision.gameObject == player)
        {
            SceneManager.LoadScene("GameOver");
        }
    }


    private void saysOw()
    {
        print("AU");
    }
}
