using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - 3 float")]
public class GameEvent3Float : GameEvent<float, float, float> { }