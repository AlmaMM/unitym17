using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Player : MonoBehaviour
{
    public DadesPersistents data;
    bool clicked = false;
    public int vidaEscut;
    public TMPro.TextMeshProUGUI vida;
    public Slider vidaEscutSlider;

    //Estructura delegat
    /*
    public delegate void Salt();
    public event Salt saltEvent;
    */
    


    // Start is called before the first frame update
    void Start()
    {
        vidaEscut = data.vidaEscut;
    }

    // Update is called once per frame
    void Update()
    {
        vida.text = "Shield: " + vidaEscut;
        data.vidaEscut = vidaEscut;

        if (Input.GetKey(KeyCode.A))
        {
            this.transform.Rotate(0, 0, 0.5f);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.transform.Rotate(0, 0, -0.5f);
        }
    }
}
