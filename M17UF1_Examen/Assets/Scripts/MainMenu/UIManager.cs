using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    GameObject canvas;
    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.Find("Canvas");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setTheme(int theme)
    {
        switch (theme)
        {
            case 0:
                canvas.transform.GetChild(1).GetComponent<Image>().color = Color.magenta;
                break;
            case 1:
                canvas.transform.GetChild(1).GetComponent<Image>().color = Color.yellow;
                break;
            case 2:
                canvas.transform.GetChild(1).GetComponent<Image>().color = Color.cyan;
                break;
        }
    }

    public void startGame()
    {
        SceneManager.LoadScene("Main");
    }
    
}
