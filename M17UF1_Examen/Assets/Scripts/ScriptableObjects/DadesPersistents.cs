using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DadesPersistents : ScriptableObject
{
    public float tempsTranscorregut;
    public int vidaEscut;
    public float force;
    public int canvisEscena;
}
