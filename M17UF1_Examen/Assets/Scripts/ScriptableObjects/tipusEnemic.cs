using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class tipusEnemic : ScriptableObject
{
    public Color color;
    public int velocitat;
    public int damage;
}
