using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class carrega : MonoBehaviour
{
    GameEvent gameEvent;
    GameObject limit;
    public DadesPersistents persistents;
    // Start is called before the first frame update
    void Start()
    {
        persistents.canvisEscena++;
        limit = GameObject.Find("Limit").gameObject;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == limit && this.GetComponent<SpriteRenderer>().color == Color.green)
        {
            SceneManager.LoadScene("Main");
        }
    }

    private void OnMouseDown()
    {
        if(this.GetComponent<SpriteRenderer>().color == Color.green)
        {
            gameEvent.Raise();
            Destroy(this.gameObject);
            persistents.vidaEscut++;
        }
        else if(this.GetComponent<SpriteRenderer>().color == Color.red)
        {
            SceneManager.LoadScene("Main");
        }
    }
}
