using System;
using System.Collections;
using TMPro;
using UnityEngine;
using static BotonMenu;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    private GameObject[] instantiatedEnemies;
    public ScriptableEnemy[] enemies;
    public GameObject enemyPrefab;
    public GameObject remains;
    private float spawnCooldown;
    public RecursosScriptable recurso;
    BotonMenu bm;

    // Start is called before the first frame update
    void Start()
    {
        spawnCooldown = 2;
        bm = FindObjectOfType<BotonMenu>();

        instantiatedEnemies = new GameObject[enemies.Length * 100];
        int countInstances = 0;
        foreach (ScriptableEnemy e in enemies)
        {
            for (int i = countInstances; i < 100 + countInstances; i++)
            {
                GameObject newEnemy = Instantiate(enemyPrefab);
                newEnemy.GetComponent<EnemiesBehaviour>().enemy = e;
                newEnemy.GetComponent<SpriteRenderer>().sprite = e.sprite;
                newEnemy.GetComponent<Animator>().runtimeAnimatorController = e.animator;
                instantiatedEnemies[i] = newEnemy;
                instantiatedEnemies[i].SetActive(false);
            }
            countInstances += 100;
        }
        StartCoroutine(remainingTime());
    }

    IEnumerator EnemiesSpawner()
    {
        int finish = instantiatedEnemies.Length;
        //Define during what time the spawner is working. Then end spawn and start "remainingnTime" countdown and roundCount++.
        while (finish > 0)
        {
            yield return new WaitForSeconds(spawnCooldown);
            int random = Random.Range(0, instantiatedEnemies.Length);
            instantiatedEnemies[random]?.SetActive(true);
            finish-=2;
        }
        recurso.round++;
        if(spawnCooldown > 0.3f)
        {
            spawnCooldown -= 0.2f;
        }
        bm.llamarD();
        StartCoroutine(remainingTime());
    }


    IEnumerator remainingTime()
    {
        remains.SetActive(true);
        recurso.time = 60;

        while (recurso.time > 0)
        {
            yield return new WaitForSeconds(0.1f);
            recurso.time--;
            bm.llamarD();
        }
        remains.SetActive(false);
        StartCoroutine(EnemiesSpawner());
        //empiezan oleadas.




    }
}
