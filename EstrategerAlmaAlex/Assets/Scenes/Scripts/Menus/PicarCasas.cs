using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicarCasas : MonoBehaviour
{
    public GameObject menuCartas;
    public GameObject fondoInvisible;

    public void OnMouseUp()
    {
        menuCartas.SetActive(true);
        fondoInvisible.SetActive(true);
    }

}
