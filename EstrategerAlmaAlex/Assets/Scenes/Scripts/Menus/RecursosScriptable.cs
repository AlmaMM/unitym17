using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RecursosScriptable : ScriptableObject
{
    public int woodRec = 20;
    public int metalRec = 10;
    public int goldRec = 0;
    public int foodRec = 110;
    public int hapinessRec = 100;
    public int hp = 10;
    public int time = 60;
    public int round = 0;
}
