using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TxtRecUpdate : MonoBehaviour
{
    public RecursosScriptable recurso;
    GameObject menu;


    // Start is called before the first frame update
    void Start()
    {
        menu = GameObject.Find("Canvas");
        menu.GetComponent<BotonMenu>().recursosD += updateameRecursos;
    }

    public void updateameRecursos()
    {
        if (this.transform.tag == "TxtWoodMenu")
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "wood " + recurso.woodRec + " W";

        if (this.transform.tag == "TxtMetalMenu")
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "metal " + recurso.metalRec + " M";

        if (this.transform.tag == "TxtGoldenMenu")
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "gold " + recurso.goldRec + " G";

        if (this.transform.tag == "TxtFoodMenu")
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "food " + recurso.foodRec + " F";

        if (this.transform.tag == "NumHapiness")
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "" + recurso.hapinessRec;

        if (this.transform.tag == "NumHP")
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "" + recurso.hp;

        if (this.transform.tag == "RemainsText")
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Remains " + recurso.time;

        if (this.transform.tag == "Rounds")
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Rounds " + recurso.round;

    }

}
