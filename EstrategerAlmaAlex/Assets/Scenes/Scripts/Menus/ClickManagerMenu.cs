using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ClickManagerMenu : MonoBehaviour
{

    public delegate void click();
    public event click whereClick;

    public void clicked()
    {
        whereClick?.Invoke();
        whereClick = null;
    }
}
