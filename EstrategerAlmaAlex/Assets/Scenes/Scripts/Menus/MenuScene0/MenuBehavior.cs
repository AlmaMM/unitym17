using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Image = UnityEngine.UI.Image;

public class MenuBehavior : MonoBehaviour
{

    public GameObject fondo1;
    public GameObject fondo2;

    public GameObject music;

    PlayMusic pm;

    // Start is called before the first frame update
    void Start()
    {
        pm = FindObjectOfType<PlayMusic>();
        pm.PlayMusicFunction(1);
    }


    public void accionarBtnsMenu(int option)
    {
        switch (option)
        {
            case 0:
                DontDestroyOnLoad(music);
                SceneManager.LoadScene(1);
                break;
            case 1:
                fondo1.SetActive(false);
                fondo2.SetActive(true);
                break;
            case 2:
                pm.PlayMusicFunction(1);
                break;
            case 3:
                pm.PlayMusicFunction(2);
                break;
            case 4:
                pm.PlayMusicFunction(3);
                break;
            case 5:
                fondo1.SetActive(true);
                fondo2.SetActive(false);
                break;
        }
    }
}
