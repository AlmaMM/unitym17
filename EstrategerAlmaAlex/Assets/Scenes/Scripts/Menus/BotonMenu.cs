using Microsoft.Unity.VisualStudio.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEditor.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;
using Random = UnityEngine.Random;

public class BotonMenu : MonoBehaviour
{
    public RecursosScriptable recurso;
    public ScriptableTower[] towers;
    public GameObject towerPrefab;
    public GameObject menuDesplegable;
    public GameObject menuCartas;
    public GameObject fondoInvisible;
    public GameObject remains;
    public GameObject btnConstruirWood;
    public GameObject btnConstruirMetal;
    public GameObject btnConstruirGold;
    public GameObject btnConstruirFood;
    public GameObject barraHapiness;
    public GameObject barraHapiness2;
    public GameObject barraHP;
    public CardsList CardsList;

    public Image bh1;
    public Image bh2;
    public Image bhp;

    ClickManagerMenu clickManager;



    public delegate void BotonMenuEventHandler();
    public event BotonMenuEventHandler recursosD;

    // Start is called before the first frame update
    void Start()
    {
        recurso.woodRec = 20;
        recurso.metalRec = 10;
        recurso.goldRec = 0;
        recurso.foodRec = 100;
        recurso.hapinessRec = 100;
        recurso.hp = 10;
        recurso.time = 60;
        recurso.round = 0;
        StartCoroutine(restarHapiness());
        menuDesplegable.SetActive(false);
        fondoInvisible.SetActive(false);
        menuCartas.SetActive(false);
        clickManager = GameObject.Find("Manager").GetComponent<ClickManagerMenu>();
        CardsList.list = new List<GameObject>();
    }

    public void desplegarMenu()
    {

        if (menuDesplegable.active == true)
        {
            menuDesplegable.SetActive(false);
            fondoInvisible.SetActive(false);
            if (menuCartas.active == true)
            {
                menuCartas.SetActive(false);
            }
        }
        else
        {
            menuDesplegable.SetActive(true);
            fondoInvisible.SetActive(true);
        }

    }

    public void desplegarMenuCartas()
    {
        if (menuCartas.active == true)
            menuCartas.SetActive(false);

        else menuCartas.SetActive(true);
    }

    public void fondoInvisibleBehavior()
    {

        if (fondoInvisible.active == true)
        {
            fondoInvisible.SetActive(false);
        }
        if (menuCartas.active == true)
        {
            menuCartas.SetActive(false);
        }
        if (menuDesplegable.active == true)
        {
            menuDesplegable.SetActive(false);
        }
    }
    public void sumarRecursos(string recurs)
    {
        switch(recurs)
        {
            case "Madera":
                recurso.woodRec += 10;
                break;
            case "Metal":
                recurso.metalRec += 10;
                break;
            case "Oro":
                recurso.goldRec += 10;
                break;
        }
        recursosD.Invoke();
    }


    public void restarRecursos()
    {

        if (EventSystem.current.currentSelectedGameObject.transform.tag == "BtnCrearWood" && recurso.woodRec >= 15)
        {
            recurso.woodRec -= 15;
            int r = Random.Range(0, 2);
            generateCarta(r);


        }
        if (EventSystem.current.currentSelectedGameObject.transform.tag == "BtnCrearMetal" && recurso.woodRec >= 5 && recurso.metalRec >= 10)
        {
            recurso.woodRec -= 5;
            recurso.metalRec -= 10;
            int r = Random.Range(2, 4);
            generateCarta(r);

        }
        if (EventSystem.current.currentSelectedGameObject.transform.tag == "BtnCrearGold" && recurso.goldRec >= 5 && recurso.metalRec >= 10)
        {
            recurso.goldRec -= 5;
            recurso.metalRec -= 10;
            generateCarta(4);
        }
        if (EventSystem.current.currentSelectedGameObject.transform.tag == "BtnCrearFood" && recurso.foodRec >= 1)
        {
            //rellenar hasta el maximo de hapiness restando justo lo necesario, o, gastar todo lo que se tenga si no llega a llenarlo
            if (100 - recurso.hapinessRec > recurso.foodRec)
            {
                recurso.hapinessRec += recurso.foodRec;
                recurso.foodRec = 0;
                barraHapiness.transform.localScale = new Vector2(recurso.hapinessRec / 100f, 1);
                barraHapiness2.transform.localScale = new Vector2(recurso.hapinessRec / 100f, 1);
            }


            if (100 - recurso.hapinessRec < recurso.foodRec)
            {
                recurso.foodRec -= 100 - recurso.hapinessRec;
                recurso.hapinessRec = 100;
                barraHapiness.transform.localScale = new Vector2(1, 1);
                barraHapiness2.transform.localScale = new Vector2(1, 1);
            }

            cambiarColorBarraFelicidad();
        }
        recursosD.Invoke();
    }




    IEnumerator restarHapiness()
    {
        while (true)
        {
            if (recurso.hapinessRec > 0)
            {
                recurso.hapinessRec -= 1;
                barraHapiness.transform.localScale = new Vector2(barraHapiness.transform.localScale.x - 0.01f, 1);
                barraHapiness2.transform.localScale = new Vector2(barraHapiness2.transform.localScale.x - 0.01f, 1);

                cambiarColorBarraFelicidad();

                recursosD.Invoke();
            }
            yield return new WaitForSeconds(5f);
        }
    }

    // mirar porque co�o se cambia el color de todas las putas imagenes y no solo las referenciadas con el puto gameobject de los huevos
    private void cambiarColorBarraFelicidad()
    {
        if (barraHapiness.transform.tag == "BarraHapiness" && barraHapiness2.transform.tag == "BarraHapiness")
        {
            if (recurso.hapinessRec < 51 && recurso.hapinessRec > 10)
            {
                bh1.GetComponent<Image>().color = new Color(241, 184, 0);
                bh2.GetComponent<Image>().color = new Color(241, 184, 0);
            }
            else if (recurso.hapinessRec < 11)
            {
                bh1.GetComponent<Image>().color = new Color(248, 0, 0);
                bh2.GetComponent<Image>().color = new Color(248, 0, 0);
            }
            else if (recurso.hapinessRec > 50)
            {
                bh1.GetComponent<Image>().color = new Color(0, 248, 0);
                bh2.GetComponent<Image>().color = new Color(0, 248, 0);
            }
        }
    }


    private GameObject generateCarta(int tower)
    {
        GameObject newCarta = Instantiate(towerPrefab);
        newCarta.transform.position = Vector3.zero;
        newCarta.GetComponent<SpriteRenderer>().sprite = towers[tower].sprite;
        newCarta.transform.GetChild(0).GetComponent<CircleCollider2D>().radius = towers[tower].rango;
        newCarta.GetComponent<Construct>().tower = towers[tower];
        return newCarta;

    }

    public void llamarD()
    {
        recursosD.Invoke();
    }

    public void restarHP()
    {

        if (recurso.hp > 0)
        {
            recurso.hp--;
            barraHP.transform.localScale = new Vector2(barraHP.transform.localScale.x - 0.1f, 1);
        }
        if (barraHP.transform.tag == "BarraHP")
        {
            if (recurso.hp < 6 && recurso.hp > 1)
            {
                bhp.GetComponent<Image>().color = new Color(241, 184, 0);
            }
            else if (recurso.hp < 2)
            {
                bhp.GetComponent<Image>().color = new Color(248, 0, 0);
            }
            else if (recurso.hp > 5)
            {
                bhp.GetComponent<Image>().color = new Color(0, 248, 0);
            }

        }
        recursosD.Invoke();

        if (recurso.hp <= 0)
        {
            SceneManager.LoadScene("Scene2");
        }
    }
}
