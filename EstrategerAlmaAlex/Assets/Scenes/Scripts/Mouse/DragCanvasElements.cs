using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragCanvasElements : MonoBehaviour, IDragHandler
{
    [SerializeField]
    private float dampingSpeed = .05f;

    private RectTransform DragObject;
    private Vector3 v = Vector3.zero;

    public void Awake()
    {
        DragObject = transform as RectTransform;
    }
    public void OnDrag(PointerEventData eventData)
    {
        if(RectTransformUtility.ScreenPointToWorldPointInRectangle(DragObject, eventData.position, 
            eventData.pressEventCamera, out var globalMousePosition))
        {
            DragObject.position = Vector3.SmoothDamp(DragObject.position, globalMousePosition, ref v, dampingSpeed);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
