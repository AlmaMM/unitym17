using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UIElements;
using Color = System.Drawing.Color;

public class GetPoint : MonoBehaviour
{
    private Vector3 pos;
    Rigidbody2D rb;
    List<Tilemap> tilemap = new List<Tilemap>();
    Tilemap tmp;
    private bool chocando;
    private Vector3 lastPosition;
    private bool wasMoved = false;

    private bool drageando;

    public void OnMouseDown()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }

    public void OnMouseDrag()
    {
        if (!this.wasMoved) 
        {
            pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos = new Vector3(pos.x, pos.y + 1, pos.z + 1);
            pos = CalculateExactlyThePoint(pos);
            rb.transform.position = pos;

            this.transform.GetChild(0).GetComponent<Collider2D>().enabled = false;
        }
        
    }

    private Vector3 CalculateExactlyThePoint(Vector3 finalPos)
    {

        int posX = (int)Math.Floor(finalPos.x);
        float finalPX = posX + 0.5f;
        finalPos.x = finalPX;

        int posY = (int)Math.Floor(finalPos.y);
        float finalPY = posY;

        if (rb.transform.tag == "TorreMadera2")
        {
            finalPY += 0.1f;
        }
        else if (rb.transform.tag == "TorreMadera1" || rb.transform.tag == "TorreMetal1")
        {
            finalPY -= 0.2f;
        }
        else if (rb.transform.tag == "TorreOro")
        {
            finalPY -= 0.1f;
        }
        else if (rb.transform.tag == "TorreMetal2")
        {
            finalPY -= 0.2f;
        }
        else if (rb.transform.tag == "Valla")
        {
            finalPY += 0.5f;
        }

        finalPos.y = finalPY;
        drageando = true;

        return finalPos;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (drageando)
        {
            if (collision.transform.tag == "Path")
            {
                tmp = collision.GetComponent<Tilemap>();
                tilemap.Add(tmp);

                /*print("posX: "+ Math.Floor(rb.transform.position.x));
                print("mouseX " + Camera.main.ScreenToWorldPoint(Input.mousePosition).x);
                print("collisionX " + collision.GetComponent<Tilemap>().WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)).x);
                print("posY "+ Math.Floor(rb.transform.position.y));
                print("mouseY " + Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
                print("collision "+ collision.GetComponent<Tilemap>().WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)).y);
                */
                if (Math.Floor(rb.transform.position.x) == collision.GetComponent<Tilemap>().WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)).x &&
                    Math.Floor(rb.transform.position.y) == collision.GetComponent<Tilemap>().WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)).y)
                {
                    for (int i = 0; i < tilemap.Count; i++)
                    {
                        tilemap[i].color = new UnityEngine.Color(248, 0, 0);
                    }
                    chocando = true;
                }
            }
            else/* if (collision.transform.tag != "Path")*/
            {
                lastPosition = rb.transform.position;
                chocando = false;
            }

        }
    }

    public void OnMouseUp()
    {
        for (int i = 0; i < tilemap.Count; i++)
        {
            tilemap[i].color = new UnityEngine.Color(255, 255, 255);
        }

        drageando = false;
        if (chocando)
        {
            rb.transform.position = lastPosition;
            chocando = false;
        }
        tilemap.Clear();
        this.transform.GetChild(0).GetComponent<Collider2D>().enabled = true;
        this.wasMoved = true;
    }
}
