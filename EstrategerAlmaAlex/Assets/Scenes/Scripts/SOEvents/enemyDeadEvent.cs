// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;

public class enemyDeadEvent : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public enemyDead Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<string> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(string recurs)
    {

        Response.Invoke(recurs);
    }
}

