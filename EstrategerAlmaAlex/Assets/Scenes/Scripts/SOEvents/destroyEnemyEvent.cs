// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

public abstract class destroyEnemyEvent<T0, T1> : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<destroyEnemyListener<T0, T1>> eventListeners =
        new List<destroyEnemyListener<T0, T1>>();

    public void Raise(T0 parameter0, T1 parameter1)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(parameter0, parameter1);
    }

    public void RegisterListener(destroyEnemyListener<T0, T1> listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(destroyEnemyListener<T0, T1> listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}


