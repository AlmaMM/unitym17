// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;

public abstract class destroyEnemyListener<T0, T1> : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public destroyEnemyEvent<T0, T1> Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<T0, T1> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(T0 parameter0, T1 parameter1)
    {
        Response.Invoke(parameter0, parameter1);
    }
}


