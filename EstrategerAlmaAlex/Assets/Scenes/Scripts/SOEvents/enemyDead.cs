// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class enemyDead : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<enemyDeadEvent> eventListeners =
        new List<enemyDeadEvent>();

    public void Raise(string recurs)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(recurs);
    }

    public void RegisterListener(enemyDeadEvent listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(enemyDeadEvent listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}

