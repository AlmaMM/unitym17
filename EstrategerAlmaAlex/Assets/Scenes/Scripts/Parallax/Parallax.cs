using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{

    public GameObject[] nube1;
    public GameObject[] nube2;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < nube1.Length; i++)
        {
            nube1[i].GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-3f, -2f), 0);
        }
        for (int i = 0; i < nube2.Length; i++)
        {
            nube2[i].GetComponent<Rigidbody2D>().velocity = new Vector2(Random.Range(-1.5f, -1f), 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < nube1.Length; i++)
        {
            if (nube1[i].transform.position.x < -30)
            {
                nube1[i].transform.position = new Vector3(30 * 1.5f, nube1[i].transform.position.y, -8);
            }
        }

        for (int i = 0; i < nube2.Length; i++)
        {
            if (nube2[i].transform.position.x < -30)
            {
                nube2[i].transform.position = new Vector3(30, nube2[i].transform.position.y, -8);
            }
        }

    }
}
