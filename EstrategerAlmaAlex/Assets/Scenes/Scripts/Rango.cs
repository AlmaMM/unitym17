using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Rango : MonoBehaviour
{
    private List<GameObject> enemyToShot = new List<GameObject>();
    IEnumerator coroutine;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (enemyToShot.Count == 0)
        {
            if (collision.tag == "Enemy")
            {
                enemyToShot.Add(collision.gameObject);
                coroutine = shoot();
                StartCoroutine(coroutine);
            }

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.transform.tag=="Enemy")
        {
            if (enemyToShot.Count == 1 && collision.gameObject == enemyToShot[0])
            {
                enemyToShot.Clear();
                StopCoroutine(coroutine);
            }
        }
        
    }


    IEnumerator shoot()
    {
        while (true)
        {
            if (enemyToShot.Count == 1)
            {
                GameObject bullet = Instantiate(GetComponentInParent<Construct>().projectilPrefab);
                bullet.GetComponent<SpriteRenderer>().sprite = this.GetComponentInParent<Construct>().tower.projectile.sprite;
                bullet.transform.position = this.transform.parent.position;
                bullet.GetComponent<Projectile>().damage = this.GetComponentInParent<Construct>().tower.projectile.damage;
                Vector3 direction = enemyToShot[0].transform.position - bullet.transform.position;
                direction.Normalize();
                bullet.GetComponent<Rigidbody2D>().velocity = direction * this.GetComponentInParent<Construct>().tower.projectile.spd;
                
            }
            yield return new WaitForSeconds(this.GetComponentInParent<Construct>().tower.shotCD);
        }
    }
}
