using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemiesBehaviour : MonoBehaviour
{
    Rigidbody2D rb;
    public enemyDead enemyDead;
    public ScriptableEnemy enemy;
    private string thisRecurs;
    private float vida;
    private List<Waypoint> waypointsUsed = new List<Waypoint>();
    private Waypoint target;
    private string recurs;
    BotonMenu bm;

    // Start is called before the first frame update
    void Start()
    {
        bm = FindObjectOfType<BotonMenu>();
    }
    private void OnEnable()
    {
        thisRecurs = enemy.recursos[Random.Range(0, 1)].recurs;
        this.vida = enemy.vida;
        int recursTipus = Random.Range(0, 2);
        switch(recursTipus)
        {
            case 0:
                this.recurs = "wood";
                break;
            case 1:
                this.recurs = "metal";
                break;
            case 2:
                this.recurs = "gold";
                break;
        }
        rb = this.GetComponent<Rigidbody2D>();
        int whatSpawn = Random.Range(0, enemy.spawnPoints.Length);
        this.GetComponent<Transform>().position = enemy.spawnPoints[whatSpawn];
        this.waypointsUsed.Clear();
        switch (whatSpawn)
        {
            case 0:
                foreach (Waypoint w in enemy.waypointsL)
                    waypointsUsed.Add(w);
                break;
            case 1:
                foreach (Waypoint w in enemy.waypointsR)
                    waypointsUsed.Add(w);
                break;
        }

        //set first target and direction
        target = waypointsUsed[0];
        Vector3 direction = (target.waypointPosition - this.transform.position);
        direction.Normalize();
        rb.velocity = direction * enemy.spd;
    }

    // Update is called once per frame
    void Update()
    {

        //Set direction updating when the last waypoint is found.
        if ((target.waypointPosition - this.transform.position).magnitude < 0.1)
        {
            waypointsUsed.Remove(target);

            if (waypointsUsed.Count > 0)
            {
                target = waypointsUsed[0];
                Vector3 direction = (target.waypointPosition - this.transform.position);
                direction.Normalize();
                rb.velocity = direction * enemy.spd;
            }
            else
            {
                this.gameObject.SetActive(false);
                bm.restarHP();

            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Projectil")
        {
            dano(collision.gameObject);
        }
    }
    private void OnDisable()
    {
        enemyDead.Raise(this.thisRecurs);
    }

    public void dano(GameObject pr)
    {

        this.vida = this.vida-pr.GetComponent<Projectile>().damage;
        print(this.vida + " " + pr.GetComponent<Projectile>().damage);
        this.transform.GetChild(0).transform.localScale -= new Vector3(1-(0.7f/pr.GetComponent<Projectile>().damage), 0, 0);
        if (this.vida <= 0f)
        {
            deactivate();
        }
        
    }


    void deactivate()
    {

        this.gameObject.SetActive(false);

    }

}
