using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CardsList : ScriptableObject
{
    public List<GameObject> list;
}
