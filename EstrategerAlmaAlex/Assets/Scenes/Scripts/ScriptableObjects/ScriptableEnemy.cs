using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableEnemy : ScriptableObject
{
    public Vector3[] spawnPoints;
    public List<Waypoint> waypointsR;
    public List<Waypoint> waypointsL;
    public RuntimeAnimatorController animator;
    public RecursScriptable[] recursos;
    public Sprite sprite;
    public int vida;
    public int spd;
}
