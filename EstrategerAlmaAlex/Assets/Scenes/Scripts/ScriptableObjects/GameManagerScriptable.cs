using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
[CreateAssetMenu]
public class GameManagerScriptable : ScriptableObject
{
    public GameObject[] possibleEnemies;
    public TextMeshProUGUI remainingTimeText;
    public Sprite[] sprites;
    public RuntimeAnimatorController[] animators;




}
