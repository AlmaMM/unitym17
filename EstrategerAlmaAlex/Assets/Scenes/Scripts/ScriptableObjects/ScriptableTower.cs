using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class ScriptableTower : ScriptableObject
{
    public ProjectilScriptable projectile;
    public Sprite sprite;
    public float shotCD;
    public float rango;
}
