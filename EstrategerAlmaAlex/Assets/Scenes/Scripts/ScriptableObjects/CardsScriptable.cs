using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CardsScriptable : ScriptableObject
{
    public GameObject cardPrefab;
    private string cardType;
    public Sprite[] possibleTowers;
}
