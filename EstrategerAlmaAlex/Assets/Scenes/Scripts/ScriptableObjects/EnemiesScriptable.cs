using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class EnemiesScriptable : ScriptableObject
{
    //ScriptableObject que servir� de plantilla para generar a los enemigos (de esta manera solo necesitamos UN prefab y varios ScriptableObjects
    public Vector3[] spawnPoints;
    public List<Waypoint> waypointsR;
    public List<Waypoint> waypointsL;
    public Sprite sprite; 
    public int vida;
    public int spd;


}
