using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ClickManagerMenu;

public class PlayMusic : MonoBehaviour
{
    AudioSource audioSource;

    public AudioClip clip1;
    public AudioClip clip2;
    public AudioClip clip3;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayMusicFunction(int option)
    {
        switch (option)
        {
            case 1:
                audioSource.Stop();
                audioSource.PlayOneShot(clip1);
                break;
            case 2:
                audioSource.Stop();
                audioSource.PlayOneShot(clip2);
                break;
            case 3:
                audioSource.Stop();
                audioSource.PlayOneShot(clip3);
                break;
        }
    }
}
