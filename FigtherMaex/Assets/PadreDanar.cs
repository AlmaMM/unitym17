using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadreDanar : MonoBehaviour
{
    public float ataque, defensa, vida;
    public bool isPLayerOne;
    Coroutine CorrutinaGuardada;

    public float Danar(float potenciaAtaque, float ataque, bool dir, float t)
    {
        int direccion = 0;
        if (dir) direccion = -1;
        if (!dir) direccion = 1;
        float vidaASumar = (this.defensa / ataque) * potenciaAtaque;
        this.vida += vidaASumar;
        if(CorrutinaGuardada != null)
        {
            StopCoroutine(CorrutinaGuardada);
        }
        CorrutinaGuardada = StartCoroutine(aplicarFuerza(t, direccion));
        return vidaASumar;
    }

    IEnumerator aplicarFuerza(float t, int direccion)
    {
        yield return new WaitForSeconds(t);
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(this.vida * 100 * direccion, this.vida * 100));
    }

}
