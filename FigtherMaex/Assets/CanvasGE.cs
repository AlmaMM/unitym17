using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CanvasGE : MonoBehaviour
{
    float intermedia;
    public TextMeshProUGUI hp1, hp2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void actualizarVidaPlayer2(float valor, bool po)
    {
        if(po)
        {
            intermedia += valor;
            hp2.text = intermedia+"%";
        }
        else
        {
            intermedia += valor;
            hp1.text = intermedia + "%";
        }
        
    }
}
