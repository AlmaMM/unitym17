using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MPSwitch : MonoBehaviour
{
    private PlayerInputManager pim;
    public GameObject[] go;
    public SOMenu SOmenu;

    // Start is called before the first frame update
    void Start()
    {
        pim = GetComponent<PlayerInputManager>();
        if (SOmenu.player1 == 1)
        {
            pim.playerPrefab = go[0];
        }
        else
        {
            pim.playerPrefab = go[1];
        }
    }

    public void changePrefab(PlayerInput pi)
    {
        if(pim.playerPrefab == go[0])
        {
            pim.playerPrefab = go[1];
        }
        else
        {
            pim.playerPrefab = go[0];
        }
    }
}
