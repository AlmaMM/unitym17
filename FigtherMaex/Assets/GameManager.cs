using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using static UnityEngine.GraphicsBuffer;

public class GameManager : MonoBehaviour
{
    public SOMenu SOMenu;
    public GameObject PFGhost, PFCube;
    private GameObject CharacterPractice, player1, player2;
    public Canvas Canvas;
    private Canvas c;
    PlayerInputManager one, two;

    // Start is called before the first frame update
    void Start()
    {
        c = Instantiate(Canvas);


        if (SOMenu.practice)
        {
            if (SOMenu.player1 == 1)
            {
                CharacterPractice = Instantiate(PFGhost);
                CharacterPractice.GetComponentInChildren<SM>().isPLayerOne = true;
                SOMenu.CH1Choosen = true;
                SOMenu.transformCh1 = CharacterPractice.transform;
                SOMenu.CH2Choosen = false;
            }
            else if (SOMenu.player1 == 2)
            {
                CharacterPractice = Instantiate(PFCube);
                CharacterPractice.GetComponentInChildren<SM>().isPLayerOne = true;
                SOMenu.transformCh2 = CharacterPractice.transform;
                SOMenu.CH1Choosen = false;
                SOMenu.CH2Choosen = true;
            }

            CharacterPractice.GetComponent<Rigidbody2D>().transform.position = new Vector2(+3.8f, +1.5f);

        }
        else
        {
            if (!SOMenu.practice)
            {
                if (SOMenu.player1 == 1)
                {
                    player1 = Instantiate(PFGhost);
                    player1.GetComponentInChildren<SM>().isPLayerOne = true;
                    player1.GetComponent<Rigidbody2D>().transform.position = new Vector2(-5, -4);
                }
                else if (SOMenu.player1 == 2)
                {
                    player1 = Instantiate(PFCube);
                    player1.GetComponent<movement>().isPLayerOne = true;
                    player1.GetComponent<Rigidbody2D>().transform.position = new Vector2(-5, -4);
                }
                if (SOMenu.player2 == 1)
                {
                    player2 = Instantiate(PFGhost);
                    player2.GetComponentInChildren<SM>().isPLayerOne = false;
                    player2.GetComponent<Rigidbody2D>().transform.position = new Vector2(5, -4);
                }
                else if (SOMenu.player2 == 2)
                {
                    player2 = Instantiate(PFCube);
                    player2.GetComponent<movement>().isPLayerOne = false;
                    player2.GetComponent<Rigidbody2D>().transform.position = new Vector2(5, -4);
                    player2.GetComponent<Rigidbody2D>().transform.localScale = new Vector2(player2.GetComponent<Rigidbody2D>().transform.localScale.x * -1,
                        player2.GetComponent<Rigidbody2D>().transform.localScale.y);
                }

            }
            StartCoroutine(UpdatePocho());
        }

    }
    IEnumerator UpdatePocho()
    {
        while(true)
        {
            yield return new WaitForSeconds(1);
            if (player1.transform.position.magnitude - new Vector3(0, 0, 0).magnitude > 14)
            {
                Destroy(player1.gameObject);
                Destroy(player2.gameObject);
                SceneManager.LoadScene(4);
            }
            else if (player2.transform.position.magnitude - new Vector3(0, 0, 0).magnitude > 14)
            {
                Destroy(player1.gameObject);
                Destroy(player2.gameObject);
                SceneManager.LoadScene(4);
            }
        }
    }
}
