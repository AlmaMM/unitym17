using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class LerpCamera : MonoBehaviour
{
    public SOMenu SOMenu;
    Transform target;

    // Start is called before the first frame update
    void Start()
    {
        if (SOMenu.CH1Choosen)
        {
            target = SOMenu.transformCh1;
        }
        else if (SOMenu.CH2Choosen)
        {
            target = SOMenu.transformCh2;
        }

        StartCoroutine(LerpPositionAmbSlowDown(4));



    }

    // Update is called once per frame
    void Update()
    {
  
    }

    IEnumerator LerpPositionAmbSlowDown(float duration)
    {
        float time = 0;
        yield return new WaitForSeconds(1);
        if (SOMenu.CH1Choosen)
        {
            target = SOMenu.transformCh1;
        }
        else if (SOMenu.CH2Choosen)
        {
            target = SOMenu.transformCh2;
        }

        while (true)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x, target.position.y+3, -10), Time.deltaTime);
            time += Time.deltaTime;
            yield return null;
        }
    }
}


