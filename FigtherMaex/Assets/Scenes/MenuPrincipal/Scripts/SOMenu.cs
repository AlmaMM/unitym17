using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SOMenu : ScriptableObject
{
    public GameObject CH1, CH2;
    public bool CH1Choosen, CH2Choosen;

    public Transform transformCh1, transformCh2;

    public int player1, player2;

    public bool practice = false;

    public float hpCH1, hpCH2;
}
