using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropDownPractice : MonoBehaviour
{
    public SOMenu SOMenu;
    public Image ch1;
    public Sprite gl, cl;

    // Start is called before the first frame update
    void Start()
    {
        SOMenu.player1 = 1;
        SOMenu.practice = true;
    }

    public void cambiarImagen(int value)
    {
        if (value == 0)
        {
            ch1.sprite = gl;
            SOMenu.player1 = 1;
            SOMenu.practice = true;
        }
        else if (value == 1)
        {
            ch1.sprite = cl;
            SOMenu.player1 = 2;
            SOMenu.practice = true;
        }
    }
}
