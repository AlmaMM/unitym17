using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MenuBehavior : MonoBehaviour
{
    public GameObject fondoOptions;
    public GameObject fondoMain;
    public GameObject fondoChoose;
    public GameObject fondoMaps;
    public GameObject fondoEspera;

    public GameObject music;

    public GameObject firstMenu, firstChoose, firstMap, firstOptions;

    PlayMusic pm;
    // Start is called before the first frame update
    void Start()
    {
        pm = FindObjectOfType<PlayMusic>();
    }

    public void PlayMusic(int option)
    {
        switch (option)
        {
            case 0:
                if (fondoMain.active == true)
                {
                    pm.PlayMusicFunction(2);
                    fondoMain.SetActive(false);
                    fondoChoose.SetActive(true);

                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstChoose);

                }
                else
                {
                    pm.PlayMusicFunction(3);
                    fondoMain.SetActive(true);
                    fondoChoose.SetActive(false);

                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstMenu);
                }
                pm.PlayMusicFunction(1);
                break;
            case 1:
                pm.PlayMusicFunction(1);
                if (fondoOptions.active == false)
                {
                    fondoOptions.SetActive(true);
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstOptions);
                }

                else
                {
                    fondoOptions.SetActive(false);

                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstMenu);
                }

                break;
            case 2:
                pm.PlayMusicFunction(1);
                if (fondoChoose.active == true)
                {
                    pm.PlayMusicFunction(4);
                    fondoChoose.SetActive(false);
                    fondoMaps.SetActive(true);


                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstMap);
                }
                else
                {
                    fondoChoose.SetActive(true);
                    fondoMaps.SetActive(false);


                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstChoose);
                }
                break;
            case 3:
                fondoEspera.SetActive(true);
                pm.PlayMusicFunction(5);
                StartCoroutine(Wait1());
                break;
            case 4:
                fondoEspera.SetActive(true);
                pm.PlayMusicFunction(6);
                StartCoroutine(Wait2());
                break;
            case 5:
                fondoEspera.SetActive(true);
                pm.PlayMusicFunction(7);
                StartCoroutine(Wait3());
                break;
        }
    }

    IEnumerator Wait1()
    {
        yield return new WaitForSeconds(3.1f);
        DontDestroyOnLoad(music);
        SceneManager.LoadScene(1);
    }

    IEnumerator Wait2()
    {
        yield return new WaitForSeconds(3.1f);
        DontDestroyOnLoad(music);
        SceneManager.LoadScene(2);
    }

    IEnumerator Wait3()
    {
        yield return new WaitForSeconds(3.1f);
        DontDestroyOnLoad(music);
        SceneManager.LoadScene(3);
    }
}
