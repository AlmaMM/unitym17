using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Jump : State
{
    string m_Animation;
    public Jump(FSM fsm, string animation) : base(fsm)
    {
        this.m_Animation = animation;
    }

    public override void Init()
    {
        jump_action();
        m_playerInput["Simple_Attk"].performed += air_attk_action;
        m_playerInput["Shoot"].performed += shoot_action;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
    }


    public override void FixedUpdate()
    {
        if (m_FSM.Owner.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            m_FSM.ChangeState<Fall>();
        }
    }

    void shoot_action(InputAction.CallbackContext obj)
    {
        m_FSM.ChangeState<Shoot>();
    }
    private void air_attk_action(InputAction.CallbackContext obj)
    {
        m_FSM.ChangeState<Air_Attk>();
    }

    void jump_action()
    {
        m_FSM.Owner.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 50), ForceMode2D.Impulse);
        m_FSM.Owner.GetComponent<movement>().m_HasJumped = true;
    }

    public override void Exit()
    {
        m_playerInput["Simple_Attk"].performed -= air_attk_action;
        m_playerInput["Shoot"].performed -= shoot_action;

    }
}
