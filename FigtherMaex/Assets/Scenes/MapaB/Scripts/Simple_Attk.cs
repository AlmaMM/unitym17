using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Simple_Attk : State
{

    string m_Animation;
    float m_PotensiaAcsoluta;
    float m_StateLength;
    Coroutine StateLength;
    float m_DelayTime;

    public Simple_Attk(FSM fsm, string animation, float stateLength, float potensiaAcsoluta, float delayTime) : base(fsm)
    {
        m_Animation = animation;
        m_StateLength = stateLength;
        m_PotensiaAcsoluta = potensiaAcsoluta;
        m_DelayTime = delayTime;
    }

    public override void Init()
    {
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_FSM.Owner.GetComponent<movement>().m_DelayTime = m_DelayTime;
        m_FSM.Owner.GetComponent<movement>().m_PotensiaAcsoluta = m_PotensiaAcsoluta;
        m_playerInput["Simple_Attk"].performed += combo_action;
        m_playerInput["Shoot"].performed += combo_shoot_action;
        StateLength = m_FSM.Owner.GetComponent<movement>().StartCoroutine(stateDuration());
    }

    private void combo_shoot_action(InputAction.CallbackContext obj)
    {
        m_FSM.ChangeState<Simple_attk_shoot>();
    }

    void combo_action(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<Simple_Attk_2>();
    }

    public override void Exit()
    {
        m_FSM.Owner.GetComponent<movement>().StopCoroutine(StateLength);
        m_playerInput["Simple_Attk"].performed -= combo_action;
        m_playerInput["Shoot"].performed -= combo_shoot_action;

    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
    IEnumerator stateDuration()
    {
        yield return new WaitForSeconds(m_StateLength);
        m_FSM.ChangeState<Idle>();
    }


}
