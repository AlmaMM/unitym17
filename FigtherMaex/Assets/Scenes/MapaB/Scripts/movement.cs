using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.IO.LowLevel.Unsafe;
using UnityEditor.Rendering.LookDev;
using UnityEngine;
using UnityEngine.InputSystem;

public class movement : PadreDanar
{
    
    private State m_CurrentState;
    private Animator m_Animator;
    private FSM m_FSM;
    public GameObject Projectile;
    public GameEvent GEO;
    public List<GameObject> m_Ammo;
    public bool m_HasJumped;
    public bool m_isGrounded;
    public float m_PotensiaAcsoluta;
    public float m_DelayTime;


    void Awake()
    {
        this.vida = 0;
        this.ataque = 8;
        this.defensa = 7;

        for(int i=0; i<50; i++)
        {
            GameObject bullet = Instantiate(Projectile);
            bullet.transform.position = new Vector3(-10, -5, 0);
            bullet.SetActive(false);
            m_Ammo.Add(bullet);
        }
        m_PotensiaAcsoluta = 0;
        m_DelayTime = 0;
        m_Animator = GetComponent<Animator>();
        m_FSM = new FSM(gameObject);
        this.GetComponent<PlayerInput>().actions["Move"].performed += move;
        m_FSM.AddState(new Idle(m_FSM, "Idle_cube"));
        m_FSM.AddState(new Jump(m_FSM, "jump_cube"));
        m_FSM.AddState(new Fall(m_FSM, "fall_cube"));
        m_FSM.AddState(new Walk(m_FSM, "walk"));
        m_FSM.AddState(new Shoot(m_FSM, "shoot", 0.45f));
        m_FSM.AddState(new Up_Air_Attk(m_FSM, "up_air_attk", 1f, 3, 0.1f));
        m_FSM.AddState(new Air_Attk(m_FSM, "air_attk", 0.9f,2,0.3f));
        m_FSM.AddState(new Simple_Attk(m_FSM, "simple_attk", 0.4f,1f,0.3f));
        m_FSM.AddState(new Simple_Attk_2(m_FSM, "simple_attk_2",0.5f,2f, 0.4f));
        m_FSM.AddState(new Simple_Attk_3(m_FSM, "simple_attk_3", 0.8f,3f, 0.2f));
        m_FSM.AddState(new Simple_attk_shoot(m_FSM, "simple_attk_shoot", 0.8f,2f,0.4f));
        m_FSM.AddState(new Simple_Attack_Shoot_Shoot(m_FSM, "simple_attk_shoot_shoot", 1.2f,4f,0.1f));
        m_FSM.ChangeState<Idle>();

    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Rigidbody2D>().velocity.y != 0)
        {
            m_isGrounded = false;
        }
        else
        {
            m_isGrounded = true;
            m_HasJumped = false;
        }
        m_FSM.Update();
       

    }

    //Esta funci�n es pura simpat�a
    public void diHola()
    {
        print("HOLA");
    }

    private void FixedUpdate()
    {
        m_FSM.FixedUpdate();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        m_FSM.OnTriggerEnter2D(collision.GetComponentInChildren<Collider2D>());
    }



    public void move(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            m_FSM.ChangeState<Walk>();
            walk(context.ReadValue<Vector2>());
        }
        if (context.canceled)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            m_FSM.ChangeState<Idle>();
        }
    }

    void walk(Vector2 v)
    {
        if (v.x < 0)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(v.x * 4, this.GetComponent<Rigidbody2D>().velocity.y);
       
    }
}
