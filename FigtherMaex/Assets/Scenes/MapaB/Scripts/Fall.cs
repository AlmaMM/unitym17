using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.InputSystem;

public class Fall : State
{
    string m_Animation;
    public Fall(FSM fsm, string animation) : base(fsm)
    {
        this.m_Animation = animation;
    }

    public override void Init()
    {
        m_playerInput["Simple_Attk"].performed += air_attk_action;
        m_playerInput["Shoot"].performed += shoot_action;
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
    }


    void shoot_action(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<Shoot>();
    }

    private void air_attk_action(InputAction.CallbackContext obj)
    {
        m_FSM.ChangeState<Air_Attk>();
    }

    public override void FixedUpdate()
    {
        if (m_FSM.Owner.GetComponent<Rigidbody2D>().velocity.y == 0)
        {
            m_FSM.ChangeState<Idle>();
        }
    }
    public override void Exit()
    {
        m_playerInput["Simple_Attk"].performed -= air_attk_action;
        m_playerInput["Shoot"].performed -= shoot_action;
    }
}
