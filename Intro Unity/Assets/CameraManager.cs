using System.Collections;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public GameObject mainCharacter;
    int pointer =0;
    public GameObject bg;
    public Transform spawnTransform;
    public GameObject[] enemiesArray;
    public GameObject[] fuel;
    private float maxY = 2;
    private float minY = -1;
    private float yPos;
    public float fuelCooldown;
    public float cooldown;
    public float difficultyCooldown;
    private float xPos;




    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(minionSpawner());
        StartCoroutine(fuelSpawner());
        StartCoroutine(difficultyController());
    }

    // Update is called once per frame
    void Update()
    {
        if (mainCharacter.transform.position.y >= maxY)
        {
            yPos = maxY;
        }
        else if (mainCharacter.transform.position.y <= minY)
        {
            yPos = minY;
        }
        else
        {
            yPos = mainCharacter.transform.position.y;
        }
        if(mainCharacter.transform.position.x <= 0)
        {
            xPos = 0;
        }
        else
        {
            xPos = mainCharacter.transform.position.x;
        }
        this.transform.position = new Vector3(xPos, yPos, this.transform.position.z);

        if (this.transform.position.x >= pointer)
        {
            this.pointer += 25;
            GameObject newBg = Instantiate(bg);
            newBg.transform.position = new Vector2(this.pointer, 0);
        }




    }
    IEnumerator fuelSpawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(fuelCooldown);
            GameObject newFuel = Instantiate(fuel[Random.Range(0,2)]);
            if (newFuel.transform.tag == "Fuel")
            {
                newFuel.transform.position = new Vector2(this.pointer + 20, Random.Range(-3, 4));
            }
            else
            {
                newFuel.transform.position = new Vector2(this.pointer + 20, this.spawnTransform.position.y);
            }
           
            
        }
    }


    IEnumerator minionSpawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(cooldown);

            GameObject newMinion = Instantiate(enemiesArray[Random.Range(0,2)]);
            if(newMinion.transform.tag == "neuron1")
            {
                newMinion.transform.position = new Vector2(this.pointer+20, this.spawnTransform.position.y+(Random.Range(0,20)));
            }
           
        }    
    }


    IEnumerator difficultyController()
    {
        while (true)
        {
            yield return new WaitForSeconds(difficultyCooldown);
            if (cooldown > 1)
            { 
                this.cooldown-=0.5f;              
            }
            this.fuelCooldown++;

        }
    }
}
