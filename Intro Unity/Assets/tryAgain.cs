using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using Button = UnityEngine.UI.Button;

public class tryAgain : MonoBehaviour
{
    private Button btn;
    // Start is called before the first frame update
    void Start()
    {
        btn = this.GameObject().GetComponent<Button>();
        btn.onClick.AddListener(reloadGame);
    }





    // Update is called once per frame
    void Update()
    {

    }

    void reloadGame()
    {
        SceneManager.LoadScene("SampleScene");
    }



}
