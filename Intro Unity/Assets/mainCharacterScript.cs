using System.Collections;
using TMPro;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class mainCharacterScript : MonoBehaviour
{
    public int fuel;
    public int upSpeed;
    Rigidbody2D rb;
    public TextMeshProUGUI fuelText;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        fuelText.text = "Fuel: " + fuel * 10;
        rb.velocity = new Vector2(30, rb.velocity.y + 20);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (rb.velocity.y < 5 && this.fuel > 0)
            {
                rb.AddForce(new Vector2(0, 2));
                rb.velocity = new Vector2(rb.velocity.x, upSpeed);
                fuel--;
                this.fuelText.text = "Fuel: " + fuel*10;

            }

        }


        if (rb.velocity.x == 0)
        {

            SceneManager.LoadScene("GameOver");

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Fuel")
        {
            Destroy(collision.gameObject);
            this.fuel += 3;
            this.fuelText.text = "Fuel: " + fuel*10;

        }
        if (collision.transform.tag == "neuron1")
        {
            Destroy(collision.gameObject);
            if (rb.velocity.x > 0 && rb.velocity.y > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x - 5, rb.velocity.y - 5);
            }

        }
        if (collision.transform.tag == "Trampoline")
        {
            rb.velocity = (new Vector2(rb.velocity.x + 6, rb.velocity.y + 15));
        }

    }



}
