using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenWalls : MonoBehaviour
{
    public GameObject a;
    public GameObject b;
    private bool open = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
         if(open)
         {
            print("AAA");
              /* a.transform.position = new Vector3(a.transform.position.x - 0.02f, a.transform.position.y, a.transform.position.z);
               b.transform.position = new Vector3(b.transform.position.x + 0.02f, b.transform.position.y, b.transform.position.z);*/
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        print("CHOQUE");
        if(other.transform.parent.transform.tag == "Player")
        {
            open = true;
            StartCoroutine(openDoors());
            GetComponent<BoxCollider>().enabled = false;
        }
    }

    IEnumerator openDoors()
    {
        float time = 300;
        while(time>0)
        {
            a.transform.position = new Vector3(a.transform.position.x - 0.02f, a.transform.position.y, a.transform.position.z);
            b.transform.position = new Vector3(b.transform.position.x + 0.02f, b.transform.position.y, b.transform.position.z);
            time--;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
