using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;
using Cursor = UnityEngine.Cursor;

public class CameraController : MonoBehaviour
{
    public GameObject m_Target;
    public GameObject m_Capsule;
    private bool m_Moving;
    private bool m_PhysicsOn = false;
    private float m_Rx;
    private float m_Ry;
    // Start is called before the first frame update
    void Start()
    {
        m_Capsule.transform.position = m_Target.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        m_Capsule.transform.position = m_Target.transform.position;
        if (m_Moving)
        {
            m_Rx = m_Capsule.transform.localEulerAngles.y + Mouse.current.delta.x.ReadValue();
            m_Ry = m_Capsule.transform.localEulerAngles.x + Mouse.current.delta.y.ReadValue();
            print("angle before clamping: " + m_Ry);
            print(m_Capsule.transform.localEulerAngles.x);
           /* if(m_Ry <= 360 || m_Ry >= 90)
                m_Capsule.transform.localEulerAngles = new Vector3(-m_Ry, m_Rx, 0);
            else
                m_Capsule.transform.localEulerAngles = new Vector3(m_Ry, m_Rx, 0);*/
            m_Ry = Mathf.Clamp(m_Ry, 0, 90);
            //print("angle after clamping: " + m_Ry);
            m_Capsule.transform.localEulerAngles = new Vector3(m_Ry, m_Rx, 0);
            transform.position = m_Capsule.transform.GetChild(0).transform.position;
        }
        else
        {
            m_Capsule.transform.localEulerAngles = Vector3.zero;
        }
        if(!m_PhysicsOn)
        {
            CameraLerp(m_Capsule.transform.GetChild(0).transform.position);
        }
        

    }

    private void CameraLerp(Vector3 targetPos)
    {
        this.transform.position = Vector3.Lerp(this.transform.position, targetPos, 2f*Time.deltaTime);
        this.transform.LookAt(m_Capsule.transform);

    }


    public void OnCameraMove(InputAction.CallbackContext c)
    {
        if(c.performed)
        {
            m_Moving = true; 
            HideAndLockCursor();
        }
        

        if (c.canceled)
        {
            m_Moving = false; 
            ShowAndUnlockCursor();
        }
           
    }

    public void OnClick(InputAction.CallbackContext c)
    {
        if(c.performed)
        {
            m_PhysicsOn = true;
        }
        if(c.canceled)
        {
            m_PhysicsOn = false;
        }
    }

    void ShowAndUnlockCursor()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    void HideAndLockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }


    public void changeTarget(GameObject newTarget)
    {
        m_Target = newTarget;
        m_Capsule.transform.position = m_Target.transform.position;
    }
}
