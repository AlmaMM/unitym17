using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityCam : MonoBehaviour
{
    public GameEvent playerAlert;
    private GameObject cast;
    // Start is called before the first frame update
    void Start()
    {
        cast = new GameObject();
        cast.AddComponent<SphereCollider>();
        cast.GetComponent<SphereCollider>().radius = 5;
        cast.transform.position = this.transform.position;
        cast.GetComponent<SphereCollider>().isTrigger = true;
    }

    // Update is called once per frame
    void Update()
    {
        //cast.transform.position += this.transform.forward*0.05f;
        RaycastHit camera;
        if (Physics.SphereCast(this.transform.position, 1, this.transform.forward, out camera))
        {
            if (camera.transform.tag == "Player")
            {
                playerAlert.Raise(camera.transform.gameObject);
            }
        }

    }
}
