using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    private Rigidbody m_Rb;
    private Light m_Light;
    private GameObject m_Target;
    private GameObject m_destroyBody;
    private Vector2 m_Movement;
    public GameEvent Possession;
    private bool m_ActivePhysics = false;
    public bool m_Grounded;
    public float m_Spd;
    public float m_JumpForce;
    private bool m_IsMoving = false;
    private float m_MinibodyCD;
    private Coroutine m_MinibodyCoroutine;
    private bool m_CanPossess = false;
    // Start is called before the first frame update
    void Start()
    {
        m_Grounded = false;
        m_Rb = transform.parent.GetComponent<Rigidbody>();
        AddLight();
        m_Light = transform.parent.GetComponent<Light>();
        m_Light.enabled = false;
        GetComponent<SphereCollider>().radius = transform.parent.GetComponent<SphereCollider>().radius+0.5f;
    }

    private void Update()
    {
        if (m_Rb.velocity.y < 0 || m_Rb.velocity.y > 0)
            m_Grounded = false;
        else
            m_Grounded = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        if (m_IsMoving && !m_ActivePhysics)
        {
            if(m_Movement.y!=0 && m_Grounded)
                m_Rb.velocity = transform.parent.transform.forward * m_Spd * m_Movement.y;

            float rotation = transform.parent.transform.localEulerAngles.y + m_Movement.x * m_Spd;
            transform.parent.transform.localEulerAngles = new Vector3(0, rotation, 0);
            //m_Rb.AddTorque(new Vector3(m_Movement.y,0,-m_Movement.x) * m_Spd, ForceMode.Acceleration);
        }
        else if(m_ActivePhysics)
        {
           // transform.parent.transform.position = new Vector3(transform.parent.transform.position.x, (Vector3.up).magnitude, transform.parent.transform.position.z);
            
            m_Rb.freezeRotation = false;
            //m_Rb.useGravity = false;
            m_Rb.AddTorque(new Vector3(m_Movement.y, 0, -m_Movement.x) * m_Spd * 10, ForceMode.Acceleration);
        }
    }

    public void OnMove(InputAction.CallbackContext c)
    {
        if(c.performed)
        {
            m_IsMoving = true;
            m_Movement = c.ReadValue<Vector2>();
        }
        if(c.canceled)
        {
            m_IsMoving = false;
            m_Movement = Vector2.zero;
        }
    }

    public void OnFire(InputAction.CallbackContext c)
    {
        if(c.performed && m_Grounded)
        {
            //m_Grounded = false;
            m_Rb.AddForce(Vector3.up*m_JumpForce, ForceMode.Impulse);

        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Finish")
        {
            m_Light.enabled = true;
            m_CanPossess = true;
            m_Target = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        m_Light.enabled = false;
        m_CanPossess = false;
        m_Target = transform.parent.gameObject;
    }
    public void OnClick(InputAction.CallbackContext c)
    {
        if(c.performed /*&& m_Grounded*/)
        {
            m_ActivePhysics = true;
        }
        if(c.canceled)
        {
            m_ActivePhysics = false;
            //m_Rb.useGravity = true;
            m_Rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }
    }
    public void OnPossess(InputAction.CallbackContext c)
    {
        if(c.performed)
        {
            if(m_CanPossess)
            {
                Possess();
            }
        }
    }
    public void OnDestroyBody(GameObject body)
    {
        newMiniBody();
        Destroy(body);
    }
    public void OnReset(InputAction.CallbackContext c)
    {
        if (c.performed && m_MinibodyCoroutine==null)
        {
            newMiniBody();
        }
        
    }

    private void newMiniBody()
    {
        GameObject miniBody = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        miniBody.transform.localScale *= 0.5f;
        miniBody.transform.position = new Vector3(transform.parent.position.x, transform.parent.position.y + 2, transform.parent.position.z);
        miniBody.AddComponent<Rigidbody>();
        miniBody.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        miniBody.AddComponent<SphereCollider>();
        miniBody.transform.tag = "MiniBody";
        m_Target = miniBody;
        Possess();
        m_MinibodyCoroutine = StartCoroutine(miniBodyCD());
    }

    private void Possess()
    {
        if(m_MinibodyCoroutine!=null)
        {
            StopCoroutine(m_MinibodyCoroutine);
            Destroy(this.transform.parent.gameObject);
            m_MinibodyCoroutine = null;
        }
          
        m_Light.enabled = false;
        this.transform.parent.transform.tag = "Finish";
        this.transform.parent = m_Target.transform;
        AddLight();
        m_Rb = transform.parent.GetComponent<Rigidbody>();
        if(m_Target.transform.tag!="MiniBody")
            m_Target.transform.tag = "Player";

        GetComponent<SphereCollider>().radius = transform.parent.GetComponent<SphereCollider>().radius + 0.5f;
        this.transform.position = transform.parent.position;
        transform.localEulerAngles = Vector3.zero;
        m_Light = transform.parent.GetComponent<Light>();
        this.enabled = true;
        Possession.Raise(transform.parent.gameObject);
    }


    IEnumerator miniBodyCD()
    {
        m_MinibodyCD = 20;
        while(m_MinibodyCD>0)
        {
            yield return new WaitForSeconds(1);
            m_MinibodyCD--;
        }
        Destroy(this);
    }


   private void AddLight()
    {
        GameObject parent = this.transform.parent.gameObject;
        parent.AddComponent<Light>();
        Light light = parent.GetComponent<Light>();
        light.type = LightType.Point;
        light.range = parent.transform.localScale.magnitude*2;
        light.color = Color.magenta;
        light.intensity = 10;
    }
}
