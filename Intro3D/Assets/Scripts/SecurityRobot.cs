using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SecurityRobot : MonoBehaviour
{
    private CharacterController cc;
    private bool playerDetected = false;
    private GameObject target;
    public GameEvent destroyBody;
    public float m_Spd;
    private float m_RotateCD;
    private float m_Rotation;
    private float gravity = 9.8f;
    private Coroutine rotate;
    private Coroutine rotaCor;
    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
        cc.detectCollisions = true;
        rotate = StartCoroutine(randomDirection());

    }

    // Update is called once per frame
    void Update()
    {
        if(playerDetected)
        {
            this.transform.LookAt(target.transform);
            if(rotate!=null)
                StopCoroutine(rotate);

            if (rotaCor != null)
                StopCoroutine(rotaCor);

            rotate = null;
            rotaCor = null;
        }
        else
        {
            if(rotate==null)
            {
                rotate = StartCoroutine(randomDirection());
            }
        }
        cc.transform.eulerAngles = new Vector3(0, this.transform.eulerAngles.y, 0);
        

    }

    private void OnTriggerEnter(Collider collision)
    {
        print("OUCH");
        if(collision.transform.tag == "Player" && playerDetected)
        {
            print("DIE");
            destroyBody.Raise(collision.gameObject);
            playerDetected = false;
        }
        else if (collision.transform.tag == "Wall")
        {
            transform.Rotate(0, 180, 0);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("COLLISION");
        if (collision.transform.tag == "Wall")
        {
            transform.Rotate(0, 180, 0);
        }
    }
    private void FixedUpdate()
    {
        cc.Move(Vector3.up * -gravity);
        cc.Move(this.transform.forward * m_Spd);
    }

    public void OnPlayerAlert(GameObject target)
    {
        this.target = target;
        playerDetected = true;
    }

    IEnumerator randomDirection()
    {
        while (!playerDetected)
        {
            m_RotateCD = Random.Range(1, 4);
            m_Rotation = Random.Range(0, 2);
            bool dir;
            if(m_Rotation ==0 )
            {
                dir = false;
            }
            else
            {
                dir = true;
            }
            yield return new WaitForSeconds(m_RotateCD);
            if(rotaCor==null)
            {
                rotaCor = StartCoroutine(rota(dir));
            }
            
        }
    }

    IEnumerator rota(bool dir)
    {
        int rot = (dir) ? 5 : -5;
        for(int i=0; i<25; i++)
        {
            yield return new WaitForSeconds(0.2f);
            transform.Rotate(0, rot,0);
        }
        rotaCor = null;
    }
}
