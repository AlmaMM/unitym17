using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour
{

    Rigidbody rb;
    public float m_JumpForce;
    public float m_Spd;
    public float m_RotationSpd;
    private bool m_Grounded;
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.A))
        {
            this.transform.Rotate(new Vector3(0, -1, 0)*m_RotationSpd);
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.Rotate(new Vector3(0, 1, 0)* m_RotationSpd);
        }
        if (Input.GetKey(KeyCode.W))
        {
            rb.velocity = this.transform.forward * m_Spd;
        }
        if (Input.GetKey(KeyCode.S))
        {
            rb.velocity = -this.transform.forward * m_Spd;
        }
        if (Input.GetKey(KeyCode.Q))
        {
            rb.velocity = -this.transform.right * m_Spd;
        }
        if (Input.GetKey(KeyCode.E))
        {
            rb.velocity = this.transform.right * m_Spd;
        }
        if(m_Grounded)
        {
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //rb.velocity += this.transform.up * m_JumpForce;
                rb.AddForce(this.transform.up * m_JumpForce, ForceMode.Impulse);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Floor")
        {
            m_Grounded = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if(collision.transform.tag == "Floor")
        {
            m_Grounded = false;
        }
    }
}
