using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnWin : MonoBehaviour
{
    public GameObject sun;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        sun.GetComponent<Light>().color = Color.magenta;
        sun.GetComponent<Light>().intensity = 10;

    }
}
